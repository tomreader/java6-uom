/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.quantity.range;

import com.gitlab.thomasreader.uom.quantity.Quantity;

import java.util.Objects;

import javax.annotation.Nonnull;

/**
 * Class representing a range of quantities e.g. 0..50 metres.
 * Implementation similar to Kotlin's ClosedRange interface.
 *
 * @param <T>
 *         the type of physical quantity
 * @author Tom Reader
 * @version 0.1.0
 * @see <a href="https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.ranges/-closed-range/">
 *     Kotlin - ClosedRange</a>
 * @see <a href="https://en.wikipedia.org/wiki/Interval_(mathematics)">Wikipedia - Interval</a>
 * @since   0.1.0
 */
public class QuantityRange<T extends Quantity<T>> {

    private final Quantity<T> start;
    private final Quantity<T> end;
    private final boolean inclusiveStart;
    private final boolean inclusiveEnd;
    //private final boolean bigPrecision;

//    /**
//     * Instantiates a new Quantity range. A null start or end symbolises infinity in that direction.
//     *
//     * @param start          the start quantity
//     * @param end            the end quantity
//     * @param inclusiveStart whether the start is inclusive
//     * @param inclusiveEnd   whether the end is inclusive
//     */
//    public QuantityRange(Quantity<T> start,
//                         Quantity<T> end,
//                         boolean inclusiveStart,
//                         boolean inclusiveEnd,
//                         boolean bigPrecision) {
//
//        if (start != null & end != null) {
//            this.start = start.min(end);
//            this.end = (this.start == start) ? end : start;
//        }
//        this.inclusiveStart = inclusiveStart;
//        this.inclusiveEnd = inclusiveEnd;
//        this.bigPrecision = bigPrecision;
//    }

    /**
     * Instantiates a new Quantity range. A null start or end symbolises infinity in that direction.
     *
     * @param start          the start quantity
     * @param end            the end quantity
     * @param inclusiveStart whether the start is inclusive
     * @param inclusiveEnd   whether the end is inclusive
     */
    public QuantityRange(Quantity<T> start,
                         Quantity<T> end,
                         boolean inclusiveStart,
                         boolean inclusiveEnd) {
        //this(start, end, inclusiveStart, inclusiveEnd, false);
        if (start != null & end != null) {
            this.start = start.min(end);
            this.end = (this.start == start) ? end : start;
        } else {
            this.start = null;
            this.end = null;
        }
        this.inclusiveStart = inclusiveStart;
        this.inclusiveEnd = inclusiveEnd;
    }
    
    /**
     * Instantiates a new Quantity range where both the start and end are inclusive.
     * A null start or end symbolises infinity in that direction.
     *
     * @param start the start quantity
     * @param end   the end quantity
     */
    public QuantityRange(Quantity<T> start, Quantity<T> end) {
        this(start, end, true, true);
    }

    public Quantity<T> getStart() {
        return this.start;
    }

    public Quantity<T> getEnd() {
        return this.end;
    }

    public boolean isInclusiveStart() {
        return this.inclusiveStart;
    }

    public boolean isInclusiveEnd() {
        return this.inclusiveEnd;
    }

    private boolean satisfiesStart(Quantity<T> quantity) {
        if (this.start == null) {
            return true;
        }
        return (inclusiveStart) ? quantity.compareTo(start.to(quantity.getUnit())) >= 0 :
                quantity.compareTo(start.to(quantity.getUnit())) > 0;
    }

    private boolean satisfiesEnd(Quantity<T> quantity) {
        if (this.end == null) {
            return true;
        }
        return (inclusiveEnd) ? quantity.compareTo(end.to(quantity.getUnit())) <= 0 :
                quantity.compareTo(end.to(quantity.getUnit())) < 0;
    }

    /**
     * Checks whether {@code quantity} is within this range. If the range is reversed e.g.
     * 50 metres to 0 metres then this method will treat is as if it was 0 metres to 50 metres.
     *
     * @param quantity the quantity
     * @return true if {@code quantity} is within the range; false otherwise
     */
    public boolean contains(Quantity<T> quantity) {
        return this.satisfiesStart(quantity) && this.satisfiesEnd(quantity);

        //Quantity<T> convertedStart = this.start.to(quantity.getUnit());
        //Quantity<T> convertedEnd = this.end.to(quantity.getUnit());

//        if (bigPrecision) {
//            BigDecimal value = quantity.bigDecimalValue();
//            BigDecimal start = convertedStart.bigDecimalValue();
//            BigDecimal end = convertedEnd.bigDecimalValue();
//
//            if (start.compareTo(end) == 0) {
//                // both have to be inclusive and magnitude will have to equal converted magnitudes.
//                return (inclusiveStart && inclusiveEnd &&
//                        (value.compareTo(start) == 0));
//            }
//
//            boolean meetsStart = (inclusiveStart) ?
//                    value.compareTo(start) >= 0 :
//                    value.compareTo(start) > 0;
//            boolean meetsEnd = (inclusiveEnd) ?
//                    value.compareTo(end) <= 0 :
//                    value.compareTo(end) < 0;
//
//            return meetsStart && meetsEnd;
//        } else {
//            double value = quantity.doubleValue();
//            double start = convertedStart.doubleValue();
//            double end = convertedEnd.doubleValue();
//
//            if (Double.compare(start, end) == 0) {
//                // both have to be inclusive and magnitude will have to equal converted magnitudes.
//                return (inclusiveStart && inclusiveEnd &&
//                        (value == convertedStart.doubleValue()));
//            }
//
//            boolean meetsStart = (inclusiveStart) ?
//                    value >= convertedStart.doubleValue() :
//                    value > convertedStart.doubleValue();
//            boolean meetsEnd = (inclusiveEnd) ?
//                    value <= convertedEnd.doubleValue() :
//                    value < convertedEnd.doubleValue();
//
//            return meetsStart && meetsEnd;
//        }
    }

    
    /**
     * Symbolises the range where square brackets [] symbolise inclusive points, and rounded brackets
     * () symbolise exlusive points
     *
     * @return {@code this.start .. this.end}
     */
    @Nonnull
    @Override
    public String toString() {
        // guesstimate: 2 x brackets (1), 2 x units (6), separator (2) = 16
        return new StringBuilder(16)
                .append((this.inclusiveStart ? '[' : '('))
                .append(this.start.toString())
                .append("..")
                .append(this.end.toString())
                .append((this.inclusiveEnd ? ']' : ')'))
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuantityRange<?> that = (QuantityRange<?>) o;
        return start.equals(that.start) && end.equals(that.end)
                || start.equals(that.end) && end.equals(that.start);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                (Objects.hash(start, end) +
                Objects.hash(end, start))
        );
    }
}
