/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.util;

/**
 * Helper class for retrieving a superscript version of a given number - including negatives.
 *
 * @author Tom Reader
 * @version 0.1.0
 * @since 0.1.0
 */
public abstract class SuperscriptFormat {


    public static final char SUPERSCRIPT_MINUS = '\u207b';

    /**
     * Helper method for converting a {@code int} to a {@code String} representation of the integer
     * in superscript form.
     * <p>
     * E.g. <code>fromInt(500) == "<sup>500</sup>"</code>
     *
     * @param integer the integer
     * @return the superscript String representation of {@code integer}
     */
    public static String fromInt(int integer) {
        // solve outlier case which cannot simply be made positive as -x
        if (integer == Integer.MIN_VALUE) {
            return new StringBuilder(11)
                    .append(SUPERSCRIPT_MINUS)
                    .append(fromDigit(2))
                    .append(fromDigit(1))
                    .append(fromDigit(4))
                    .append(fromDigit(7))
                    .append(fromDigit(4))
                    .append(fromDigit(8))
                    .append(fromDigit(3))
                    .append(fromDigit(6))
                    .append(fromDigit(4))
                    .append(fromDigit(8))
                    .toString();
        }

        boolean isPositive = true;
        if (integer < 0) {
            isPositive = false;
            integer = -integer;
        }

        // single digit [0..9] no need to construct string to iterate over
        if (integer <= 9) {
            if (isPositive) {
                return Character.toString(fromDigit(integer));
            } else {
                return "" + SUPERSCRIPT_MINUS + fromDigit(integer);
            }
        } else {
            String intString = Integer.toString(integer);
            // add an extra size of 1 for "-" if negative
            int builderLength = (isPositive) ? 0 : 1 + intString.length();
            StringBuilder stringBuilder = new StringBuilder(builderLength);
            if (!isPositive) {
                stringBuilder.append(SUPERSCRIPT_MINUS);
            }
            for (int i = 0; i < intString.length(); i++) {
                // remove the offset for digits in char vs int using 0 and cast back to int
                stringBuilder.append(fromDigit((int) intString.charAt(i) - '0'));
            }
            return stringBuilder.toString();
        }
    }

    /**
     * Helper method for converting a digit ({@code int} between 0 and 9) to a {@code char}
     * representation of the digit in superscript form.
     * <p>
     * E.g. <code>fromDigit(5) == "<sup>5</sup>"</code>
     *
     * @param digit the digit
     * @return the superscript char representation of {@code digit}
     * @throws IllegalArgumentException {@code IllegalArgumentException} if the integer passed is not between {@code 0}
     *                                  and {@code 9}
     */
    public static char fromDigit(int digit) throws IllegalArgumentException {
        switch (digit) {
            case 0:
                return '\u2070';
            case 1:
                return '\u00B9';
            case 2:
                return '\u00B2';
            case 3:
                return '\u00B3';
            case 4:
                return '\u2074';
            case 5:
                return '\u2075';
            case 6:
                return '\u2076';
            case 7:
                return '\u2077';
            case 8:
                return '\u2078';
            case 9:
                return '\u2079';
            default:
                // digit is not between 0 and 9 - and so is not a single character.
                throw new IllegalArgumentException(digit + " not between the range of 0..9");
        }
    }

}
