/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.math;

import java.math.BigDecimal;

import javax.annotation.Nonnull;

/**
 * Interface modeling a function in the form f(x,y) where x and y are consumed to produce an output.
 *
 * @author Tom Reader
 * @version 0.1.0
 * @see <a href="https://en.wikipedia.org/wiki/Relative_change_and_difference#Definitions">
 *     Wikipedia - Relative change and difference #Defnitions</a>
 * @since 0.1.0
 */
public interface FunctionXY {

    /**
     * Function consuming two doubles x and y to produce an output.
     *
     * @param x x
     * @param y y
     * @return the double result
     */
    public double invoke(double x, double y);

    /**
     * Function consuming two {@link BigDecimal}s x and y to produce an output.
     *
     * @param x x
     * @param y y
     * @return the BigDecimal result
     */
    @Nonnull
    public BigDecimal invoke(@Nonnull BigDecimal x, @Nonnull BigDecimal y);

    /**
     * The constant which returns the greatest absolute value between x and y: max(|x|,|y|).
     */
    public static final FunctionXY MAX_OF_ABSOLUTES = new FunctionXY() {
        @Override
        public double invoke(double x, double y) {
            return Math.max(Math.abs(x), Math.abs(y));
        }

        @Override
        public @Nonnull BigDecimal invoke(@Nonnull BigDecimal x, @Nonnull BigDecimal y) {
            return x.abs().max(y.abs());
        }
    };

    /**
     * The constant which returns the greatest value between x and y: max(x,y).
     */
    public static final FunctionXY MAX = new FunctionXY() {
        @Override
        public double invoke(double x, double y) {
            return Math.max(x, y);
        }

        @Override
        public @Nonnull BigDecimal invoke(@Nonnull BigDecimal x, @Nonnull BigDecimal y) {
            return x.max(y);
        }
    };

    /**
     * The constant which returns the smallest absolute value between x and y: min(|x|,|y|).
     */
    public static final FunctionXY MIN_OF_ABSOLUTES = new FunctionXY() {
        @Override
        public double invoke(double x, double y) {
            return Math.min(Math.abs(x), Math.abs(y));
        }

        @Override
        public @Nonnull BigDecimal invoke(@Nonnull BigDecimal x, @Nonnull BigDecimal y) {
            return x.abs().min(y.abs());
        }
    };

    /**
     * The constant which returns the smallest value between x and y: min(x,y).
     */
    public static final FunctionXY MIN = new FunctionXY() {
        @Override
        public double invoke(double x, double y) {
            return Math.min(x, y);
        }

        @Override
        public @Nonnull BigDecimal invoke(@Nonnull BigDecimal x, @Nonnull BigDecimal y) {
            return x.min(y);
        }
    };

    /**
     * The constant which returns the average value between x and y: (x+y)/2.
     */
    public static final FunctionXY AVERAGE = new FunctionXY() {
        @Override
        public double invoke(double x, double y) {
            return (x+y) / 2;
        }

        @Override
        public @Nonnull BigDecimal invoke(@Nonnull BigDecimal x, @Nonnull BigDecimal y) {
            return x.add(y).divide(BigDecimal.valueOf(2));
        }
    };

    /**
     * The constant which returns the average value of absolutes x and y: (|x|+|y|)/2.
     */
    public static final FunctionXY AVERAGE_OF_ABSOLUTES = new FunctionXY() {
        @Override
        public double invoke(double x, double y) {
            return (Math.abs(x) + Math.abs(y)) / 2;
        }

        @Override
        public @Nonnull BigDecimal invoke(@Nonnull BigDecimal x, @Nonnull BigDecimal y) {
            return x.abs().add(y.abs()).divide(BigDecimal.valueOf(2));
        }
    };
}
