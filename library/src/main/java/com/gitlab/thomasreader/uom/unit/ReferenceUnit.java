/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.unit;

import com.gitlab.thomasreader.uom.conversion.Conversions;
import com.gitlab.thomasreader.uom.conversion.MultiplicativeConversion;
import com.gitlab.thomasreader.uom.quantity.Quantity;
import com.gitlab.thomasreader.uom.quantity.type.Dimensionless;
import com.gitlab.thomasreader.uom.unit.format.UnitSymbolFormat;

import javax.annotation.Nonnull;

/**
 * A unit which is a reference by having an {@link MultiplicativeConversion#IDENTITY} conversion.
 * Typically models a <a href="https://en.wikipedia.org/wiki/SI_base_unit">
 * SI base unit</a> but can be used for any unit of choice.
 *
 * @param <T>
 *     the quantity type
 * @author Tom Reader
 * @version 0.1.0
 * @since 0.1.0
 */
public class ReferenceUnit<T extends Quantity<T>> extends AbstractUnit<T> implements Unit<T> {


    /**
     * The dimensionless quantity of "one".
     * @see <a href="https://en.wikipedia.org/wiki/Dimensionless_quantity">Dimensionless quantity</a>
     */
    public static final Unit<Dimensionless> ONE =
            new ReferenceUnit<Dimensionless>(null, Dimensionless.class, false, null);

    private final String symbol;
    @Nonnull
    private final Class<T> type;

    public ReferenceUnit(String symbol, @Nonnull Class<T> type) {
        this(symbol, type, true, null);
    }

    public ReferenceUnit(String symbol,
                         @Nonnull Class<T> type,
                         boolean canPrefix,
                         UnitSymbolFormat symbolFormat) {
        super(canPrefix, symbolFormat);
        this.symbol = symbol;
        this.type = type;
    }

    @Override
    public String getSymbol() {
        return this.symbol;
    }

    @Nonnull
    @Override
    public Class<T> getType() {
        return this.type;
    }

    @Nonnull
    @Override
    public MultiplicativeConversion getConversion() {
        return Conversions.identity();
    }

    @Nonnull
    @Override
    public Unit<T> getReferenceUnit() {
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReferenceUnit<?> referenceUnit = (ReferenceUnit<?>) o;
        return this.getType().equals(referenceUnit.getType());
    }

    @Override
    public int hashCode() {
        return this.getType().hashCode();
    }
}
