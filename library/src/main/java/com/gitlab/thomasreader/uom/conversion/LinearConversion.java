/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;

import com.gitlab.thomasreader.uom.math.BigDecRational;

import javax.annotation.Nonnull;

/**
 * <p>
 * This interface represents a conversion of a
 * <a href="https://en.m.wikipedia.org/wiki/Linear_function">Wikipedia - Linear function</a>
 * </p>
 *
 * @author Tom Reader
 * @version 0.1.0
 * @see <a href="https://en.m.wikipedia.org/wiki/Linear_function">Wikipedia - Linear function</a>
 * @since 0.1.0
 */
public interface LinearConversion extends UnitConversion {


    /**
     * Gets the coefficient of this linear function.
     *
     * @return the coefficient
     */
    public double getCoefficient();

    /**
     * Gets the rational coefficient of this linear function.
     *
     * @return the rational coefficient
     */
    @Nonnull
    public BigDecRational getRationalCoefficient();

//    @Nonnull
//    public BigDecimal getDecimalCoefficient();
}
