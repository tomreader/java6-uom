/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.quantity.format;

import com.gitlab.thomasreader.uom.quantity.DecimalQuantity;
import com.gitlab.thomasreader.uom.quantity.Quantity;
import com.gitlab.thomasreader.uom.unit.Unit;
import com.gitlab.thomasreader.uom.unit.format.UnitSymbolFormat;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import javax.annotation.Nonnull;

/**
 * The interface for formatting Quantities.
 *
 * @author Tom Reader
 * @version 0.1.0
 * @since 0.1.0
 */
public class QuantityFormat {

    @Nonnull
    private DecimalFormat numberFormat;
    private UnitSymbolFormat unitSymbolFormat = null;
    private boolean showDelta = true;
    private static final char DELTA = '\u0394';

    /**
     * Instantiates a new Quantity format.
     */
    public QuantityFormat() {
        this(getDefaultNumberFormat(), null);
    }

    /**
     * Instantiates a new Quantity format.
     *
     * @param numberFormat     the number format
     * @param unitSymbolFormat the unit symbol format
     */
    public QuantityFormat(@Nonnull DecimalFormat numberFormat, @Nonnull UnitSymbolFormat unitSymbolFormat) {
        this.numberFormat = numberFormat;
        this.unitSymbolFormat = unitSymbolFormat;
    }

    /**
     * Instantiates a new Quantity format.
     *
     * @param numberFormat the number format
     */
    public QuantityFormat(@Nonnull DecimalFormat numberFormat) {
        this(numberFormat, null);
    }

    /**
     * Instantiates a new Quantity format.
     *
     * @param unitSymbolFormat the unit symbol format
     */
    public QuantityFormat(@Nonnull UnitSymbolFormat unitSymbolFormat) {
        this(getDefaultNumberFormat(), unitSymbolFormat);
    }

    @Nonnull
    private static DecimalFormat getDefaultNumberFormat() {
        DecimalFormat formatter = new DecimalFormat();
        formatter.setMaximumFractionDigits(16);
        DecimalFormatSymbols decimalFormatSymbols = formatter.getDecimalFormatSymbols();
        //decimalFormatSymbols.setGroupingSeparator(' ');
        formatter.setDecimalFormatSymbols(decimalFormatSymbols);
        formatter.setGroupingUsed(false);
        return formatter;
    }

    /**
     * Gets decimal format.
     *
     * @return the decimal format
     */
    @Nonnull
    public DecimalFormat getDecimalFormat() {
        return this.numberFormat;
    }

    /**
     * Sets decimal format.
     *
     * @param numberFormat the number format
     */
    public void setDecimalFormat(@Nonnull DecimalFormat numberFormat) {
        this.numberFormat = numberFormat;
    }

    /**
     * Gets unit symbol format.
     *
     * @return the unit symbol format
     */
    @Nonnull
    public UnitSymbolFormat getUnitSymbolFormat() {
        return this.unitSymbolFormat;
    }

    /**
     * Sets unit symbol format.
     *
     * @param unitSymbolFormat the unit symbol format
     */
    public void setUnitSymbolFormat(@Nonnull UnitSymbolFormat unitSymbolFormat) {
        this.unitSymbolFormat = unitSymbolFormat;
    }

    /**
     * Show delta if the {@link Quantity#isInterval()}
     *
     * @param showDelta whether to show the delta
     */
    public void showDeltaForInterval(boolean showDelta) {
        this.showDelta = showDelta;
    }

    /**
     * Formats the {@link Quantity} to a String using it's
     * {@link Unit} symbol.
     *
     * @param quantity the quantity
     * @return the formatted string
     */
    @Nonnull
    public String format(@Nonnull Quantity<?> quantity) {
        return (showDelta && quantity.isInterval()) ?
                DELTA + " " + this.simpleFormat(quantity) : this.simpleFormat(quantity);

    }

    @Nonnull
    private String simpleFormat(@Nonnull Quantity<?> quantity) {
        if (quantity instanceof DecimalQuantity) {
            return this.getDecimalFormat().format(quantity.bigDecimalValue())
                    + ((this.unitSymbolFormat == null) ?
                    quantity.getUnit().getSymbolFormat().format(quantity.getUnit()) :
                    this.unitSymbolFormat
            );
        }
        return this.getDecimalFormat().format(quantity.doubleValue())
                + ((this.unitSymbolFormat == null) ?
                quantity.getUnit().getSymbolFormat().format(quantity.getUnit()) :
                this.unitSymbolFormat
        );
    }
}
