/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.unit;

import com.gitlab.thomasreader.uom.conversion.Conversions;
import com.gitlab.thomasreader.uom.conversion.MultiplicativeConversion;
import com.gitlab.thomasreader.uom.conversion.UnitConversion;
import com.gitlab.thomasreader.uom.quantity.Quantity;
import com.gitlab.thomasreader.uom.unit.format.UnitSymbolFormat;

import javax.annotation.Nonnull;

/**
 * Builder to make construction of units easier.
 *
 * @author Tom Reader
 * @version 0.1.0
 * @since   0.1.0
 */
public class UnitBuilder {
    private String symbol;
    private UnitSymbolFormat symbolFormat = UnitSymbolFormat.DEFAULT;
    private boolean canPrefix = true;

    /**
     * Instantiates a new Unit builder.
     *
     * @param symbol the symbol
     */
    protected UnitBuilder(String symbol) {
        this.symbol = symbol;
    }

    /**
     * Instantiates a new UnitBuilder with the provided symbol.
     *
     * @param symbol the symbol
     * @return the builder
     */
    @Nonnull
    public static UnitBuilder symbol(String symbol) {
        return new UnitBuilder(symbol);
    }

    /**
     * Set the symbol.
     *
     * @param symbol the symbol
     * @return the builder
     */
    @Nonnull
    public UnitBuilder setSymbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    /**
     * Sets the symbol format.
     *
     * @param symbolFormat the symbol format
     * @return the builder
     */
    @Nonnull
    public UnitBuilder setSymbolFormat(UnitSymbolFormat symbolFormat) {
        this.symbolFormat = symbolFormat;
        return this;
    }

    /**
     * Sets whether this unit can be prefixed.
     *
     * @param canPrefix whether this unit can be prefixed
     * @return the builder
     */
    @Nonnull
    public UnitBuilder canPrefix(boolean canPrefix) {
        this.canPrefix = canPrefix;
        return this;
    }

    /**
     * Instantiates this unit as a reference unit with a type.
     *
     * @param <T>  the quantity type
     * @param type the type
     * @return the reference unit
     */
    @Nonnull
    public <T extends Quantity<T>> Unit<T> referenceUnit(@Nonnull Class<T> type) {
        return new ReferenceUnit<T>(this.symbol, type, this.canPrefix, this.symbolFormat);
    }

    /**
     * Instantiates this unit as a regular unit with a reference and conversion to the reference.
     *
     * @param <T>        the quantity type
     * @param reference  the reference unit
     * @param conversion the conversion to the reference
     * @return the unit
     */
    @Nonnull
    public <T extends Quantity<T>> Unit<T> unit(@Nonnull Unit<T> reference, @Nonnull UnitConversion conversion) {
        if (conversion instanceof MultiplicativeConversion.IdentityConversion) {
            return this.alias(reference);
        } if (!(reference instanceof ReferenceUnit)) {
            return this.unit(
                    reference.getReferenceUnit(),
                    Conversions.of(conversion, reference.getConversion())
            );
        } else {
            return new ConvertedUnit<T>(this.symbol, reference, conversion, this.canPrefix, this.symbolFormat);
        }
    }

    /**
     * Instantiates this unit as an alias for the provided unit.
     *
     * @param <T>     the quantity type
     * @param aliased the unit to be aliased
     * @return the alias unit
     */
    @Nonnull
    public <T extends Quantity<T>> Unit<T> alias(@Nonnull Unit<T> aliased) {
        return new AliasedUnit<T>(this.symbol, aliased, this.canPrefix, this.symbolFormat);
    }
}
