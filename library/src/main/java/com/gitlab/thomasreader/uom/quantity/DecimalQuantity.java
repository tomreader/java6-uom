/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.quantity;

import com.gitlab.thomasreader.uom.conversion.LinearConversion;
import com.gitlab.thomasreader.uom.conversion.MultiplicativeConversion;
import com.gitlab.thomasreader.uom.conversion.UnitConversion;
import com.gitlab.thomasreader.uom.quantity.type.Dimensionless;
import com.gitlab.thomasreader.uom.math.BigDecRational;
import com.gitlab.thomasreader.uom.math.FunctionXY;
import com.gitlab.thomasreader.uom.unit.PrefixedUnit;
import com.gitlab.thomasreader.uom.unit.ReferenceUnit;
import com.gitlab.thomasreader.uom.unit.Unit;
import com.gitlab.thomasreader.uom.unit.prefix.MetricPrefix;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.NumberFormat;

import javax.annotation.Nonnull;

/**
 * BigDecimal implementation of {@link Quantity<T>}.
 *
 * @param <T> the type of quantity
 * @author Tom Reader
 * @version 0.1.0
 * @see Quantity
 * @see <a href="https://en.wikipedia.org/wiki/Quantity">Wikipedia - Quantity</a>
 * @see <a href="http://www.dsc.ufcg.edu.br/~jacques/cursos/map/recursos/fowler-ap/Analysis%20Pattern%20Quantity.htm">     Martin Fowler - Quantity</a>
 * @since 0.1.0
 */
public class DecimalQuantity<T extends Quantity<T>> extends AbstractQuantity<T> {

    @Nonnull
    private final BigDecimal value;
    @Nonnull
    private final MathContext mathContext;

    /**
     * Instantiates a new Decimal quantity.
     *
     * @param value       the value
     * @param unit        the unit
     * @param isInterval  the is interval
     * @param mathContext the math context
     */
    public DecimalQuantity(@Nonnull BigDecimal value,
                           @Nonnull Unit<T> unit,
                           boolean isInterval,
                           MathContext mathContext) {
        super(unit, isInterval);
        this.value = value;
        this.mathContext = (mathContext == null) ? MathContext.DECIMAL128 : mathContext;
    }

    /**
     * Instantiates a new Decimal quantity.
     *
     * @param value      the value
     * @param unit       the unit
     * @param isInterval the is interval
     */
    public DecimalQuantity(@Nonnull BigDecimal value, @Nonnull Unit<T> unit, boolean isInterval) {
        this(value, unit, isInterval, MathContext.DECIMAL128);
    }

    /**
     * Instantiates a new Decimal quantity.
     *
     * @param value       the value
     * @param unit        the unit
     * @param mathContext the math context
     */
    public DecimalQuantity(@Nonnull BigDecimal value, @Nonnull Unit<T> unit, MathContext mathContext) {
        this(value, unit, false, mathContext);
    }

    /**
     * Instantiates a new Decimal quantity.
     *
     * @param value the value
     * @param unit  the unit
     */
    public DecimalQuantity(@Nonnull BigDecimal value, @Nonnull Unit<T> unit) {
        this(value, unit, false);
    }

    /**
     * Gets the math context used for conversions.
     *
     * @return the math context
     */
    @Nonnull
    public MathContext getMathContext() {
        return this.mathContext;
    }

    @Override
    public double doubleValue() {
        return this.value.doubleValue();
    }

    @Nonnull
    @Override
    public BigDecimal bigDecimalValue() {
        return this.value;
    }

    @Nonnull
    @Override
    public BigDecimal numberValue() {
        return this.bigDecimalValue();
    }

    @Nonnull
    @Override
    public String getValue(@Nonnull NumberFormat numberFormatter) {
        return numberFormatter.format(this.value);
    }

    @Override
    public int compareTo(@Nonnull Quantity<T> otherQuantity) {
        return this.bigDecimalValue().compareTo(otherQuantity.to(this.getUnit()).bigDecimalValue());
    }

    @Override
    public int compareTo(@Nonnull Quantity<T> that, double epsilon) {
        Quantity<T> convertedThat = that.to(this.getUnit());
        BigDecimal thatValue = convertedThat.bigDecimalValue();
        int cachedCompareTo = this.bigDecimalValue().compareTo(thatValue);
        if (cachedCompareTo == 0) {
            return 0;
        }
        BigDecimal relativeDiff = this.relativeDifference(convertedThat, FunctionXY.MAX_OF_ABSOLUTES).bigDecimalValue();
        if (relativeDiff.compareTo(BigDecimal.valueOf(epsilon)) <= 0) {
            return 0;
        } else {
            return cachedCompareTo;
        }
    }

    @Nonnull
    @Override
    public DecimalQuantity<T> to(@Nonnull Unit<T> target) {
        return this.to(target, this.getMathContext());
    }

    /**
     * Returns a {@code DecimalQuantity<T>} whose value is converted to the target unit.
     *
     * @param target
     *         the unit to convert to
     * @param mathContext the MathContext to use for this conversion
     * @return the {@code Quantity<T>} with the supplied {@code target}
     */
    @Nonnull
    public DecimalQuantity<T> to(@Nonnull Unit<T> target, MathContext mathContext) {
        if (target.equals(this.getUnit())) {
            return this;
        }

        BigDecimal result;
        boolean resultIsInterval = this.isInterval();

        if (this.getUnit().getConversion().equals(target.getConversion())) {
            result = this.bigDecimalValue();

        } else if (this.isInterval() && this.getUnit().getConversion() instanceof LinearConversion &&
                !(this.getUnit().getConversion() instanceof MultiplicativeConversion)) {
            BigDecimal tempResult = ((LinearConversion) this.getUnit().getConversion())
                    .getRationalCoefficient()
                    .multiply(BigDecRational.valueOf(this.bigDecimalValue()))
                    .bigDecimalValue(mathContext);

            if (target.getConversion() instanceof LinearConversion) {
                tempResult = ((LinearConversion) target.getConversion())
                        .getRationalCoefficient()
                        .reciprocal()
                        .multiply(BigDecRational.valueOf(tempResult))
                        .bigDecimalValue(mathContext);
            } else {
                // do we throw here or just convert?
                tempResult = target.getConversion().inverse(tempResult, mathContext);
                resultIsInterval = false;
            }
            result = tempResult;

        } else {
            result = target.getConversion().inverse(
                    this.getUnit().getConversion().convert(this.bigDecimalValue(), mathContext),
                    mathContext
            );
        }

        return new DecimalQuantity<T>(
                result,
                target,
                resultIsInterval,
                mathContext
        );
    }

    @Nonnull
    @Override
    public DecimalQuantity<T> add(@Nonnull Quantity<T> augend) {
        // add not same as subtract.negate() -> 10dB + 10dB ~= 13dB, 10dB - (-10dB) ~= 10.4dB
        // negative logarithmic units model just very small absolute numbers.
        // -> therefore work out result of these units or subtract.negate()
        UnitConversion thisConv = this.getUnit().getConversion();
        UnitConversion thatConv = augend.getUnit().getConversion();

        BigDecimal result;
        boolean isInterval = this.isInterval() && augend.isInterval();

        if (!(thisConv instanceof LinearConversion) ||
                !(augend.getUnit().getConversion() instanceof LinearConversion)) {

            result = thisConv.convert(this.bigDecimalValue(), this.getMathContext())
                    .add(augend.getUnit().getConversion()
                            .convert(augend.bigDecimalValue(), this.getMathContext())
                    );
            result = thisConv.inverse(result, this.getMathContext());
        } else if (thisConv instanceof MultiplicativeConversion && thatConv instanceof MultiplicativeConversion) {
            // we're multiplicative here
            result = this.bigDecimalValue().add(
                    augend.to(this.getUnit()).bigDecimalValue(), this.getMathContext()
            );
        } else {
            // derivable but not strictly multiplicative unless is an interval e.g. temp units
            LinearConversion thisConvD = (LinearConversion) thisConv;
            LinearConversion thatConvD = (LinearConversion) thatConv;

//            double thisVal = (this.isInterval()) ?
//                    thisConvD.getFactor() * this.doubleValue() :
//                    thisConvD.convert(this.doubleValue());
//
//            double thatVal = (augend.isInterval()) ?
//                    thatConvD.getFactor() * augend.doubleValue() :
//                    thatConvD.convert(augend.doubleValue());

            Unit<T> rootUnit = this.getUnit().getReferenceUnit();
            BigDecimal tempResult = this.to(rootUnit, this.getMathContext())
                    .bigDecimalValue()
                    .add(augend.to(rootUnit).bigDecimalValue(), this.getMathContext());
            result = thisConv.inverse(tempResult, this.getMathContext());
        }

        return new DecimalQuantity<T>(result, this.getUnit(), this.getMathContext());
        //return this.subtract(augend.negate());
    }

    @Nonnull
    @Override
    public DecimalQuantity<T> subtract(@Nonnull Quantity<T> subtrahend) {
        return this.subtract(subtrahend, false);
    }

    @Nonnull
    @Override
    public DecimalQuantity<T> interval(@Nonnull Quantity<T> subtrahend) {
        return this.subtract(subtrahend, true);
    }

    @Nonnull
    private DecimalQuantity<T> subtract(@Nonnull Quantity<T> subtrahend, boolean forceInterval) {
        UnitConversion thisConv = this.getUnit().getConversion();
        UnitConversion thatConv = subtrahend.getUnit().getConversion();

        BigDecimal result;
        boolean isInterval = forceInterval || (this.isInterval() && subtrahend.isInterval());

        // Not derivable e.g. exp or log unit (dB, np)
        if (!(thisConv instanceof LinearConversion) || !(thatConv instanceof LinearConversion)) {
            result = thisConv.convert(this.bigDecimalValue(), this.getMathContext())
                    .subtract(thatConv.convert(subtrahend.bigDecimalValue(), this.getMathContext()));
            result = thisConv.inverse(result, this.getMathContext());
            // cannot force a non-linear unit to become an interval?
            isInterval = false;
        } else if (thisConv instanceof MultiplicativeConversion && thatConv instanceof MultiplicativeConversion) {
            // we're multiplicative here
            result = this.bigDecimalValue()
                    .subtract(subtrahend.to(this.getUnit()).bigDecimalValue(), this.getMathContext());
        } else {
            // derivable but not strictly multiplicative unless is an interval e.g. temp units
            LinearConversion thisConvD = (LinearConversion) thisConv;
            LinearConversion thatConvD = (LinearConversion) thatConv;

            BigDecimal thisVal = (this.isInterval()) ?
                    thisConvD.getRationalCoefficient().multiply(BigDecRational.valueOf(this.bigDecimalValue()))
                            .bigDecimalValue(this.getMathContext()) :
                    thisConvD.convert(this.bigDecimalValue(), this.getMathContext());

            BigDecimal thatVal = (subtrahend.isInterval()) ?
                    thatConvD.getRationalCoefficient().multiply(BigDecRational.valueOf(subtrahend.bigDecimalValue()))
                    .bigDecimalValue(this.getMathContext()) :
                    thatConvD.convert(this.bigDecimalValue(), this.getMathContext());

            result = thisConv.inverse(thisVal.subtract(thatVal, this.getMathContext()),
                    this.getMathContext());
        }

        return new DecimalQuantity<T>(result, this.getUnit(), isInterval, this.getMathContext());
    }

    @Nonnull
    @Override
    public DecimalQuantity<T> multiply(@Nonnull BigDecimal multiplicand) {
        UnitConversion thisConv = this.getUnit().getConversion();

        BigDecimal result;
        boolean isInterval = this.isInterval();

        // Not derivable e.g. exp or log unit (dB, np)
        if (!(thisConv instanceof LinearConversion)) {
            result = thisConv.convert(this.bigDecimalValue(), this.getMathContext())
                    .multiply(multiplicand, this.getMathContext());
            result = thisConv.inverse(result, this.getMathContext());
            // cannot force a non-linear unit to become an interval?
            isInterval = false;
        } else if (thisConv instanceof MultiplicativeConversion) {
            // we're multiplicative here
            result = this.bigDecimalValue().multiply(multiplicand, this.getMathContext());
        } else {
            // derivable but not strictly multiplicative unless is an interval e.g. temp units
            LinearConversion thisConvD = (LinearConversion) thisConv;
            result = (this.isInterval()) ?
                    this.bigDecimalValue().multiply(multiplicand, this.getMathContext()) :
                    thisConv.inverse(
                            thisConv.convert(this.bigDecimalValue(), this.getMathContext())
                                    .multiply(multiplicand, this.getMathContext()),
                            this.getMathContext()
                    );
        }

        return new DecimalQuantity<T>(result, this.getUnit(), isInterval);
    }

    @Nonnull
    @Override
    public DecimalQuantity<T> multiply(double multiplicand) {
        return this.multiply(BigDecimal.valueOf(multiplicand));
    }

    @Nonnull
    @Override
    public DecimalQuantity<T> divide(@Nonnull BigDecimal dividend) throws ArithmeticException {
        if (dividend.compareTo(BigDecimal.ZERO) == 0) {
            throw new ArithmeticException("Divisor argument passed in has value 0. " +
                    "A number cannot be divided by 0.");
        }
        UnitConversion thisConv = this.getUnit().getConversion();

        BigDecimal result;
        boolean isInterval = this.isInterval();

        // Not derivable e.g. exp or log unit (dB, np)
        if (!(thisConv instanceof LinearConversion)) {
            result = thisConv.convert(this.bigDecimalValue(), this.getMathContext())
                    .divide(dividend, this.getMathContext());
            result = thisConv.inverse(result, this.getMathContext());
            // cannot force a non-linear unit to become an interval?
            isInterval = false;
        } else if (thisConv instanceof MultiplicativeConversion) {
            // we're multiplicative here
            result = this.bigDecimalValue().divide(dividend, this.getMathContext());
        } else {
            // derivable but not strictly multiplicative unless is an interval e.g. temp units
            LinearConversion thisConvD = (LinearConversion) thisConv;
            result = (this.isInterval()) ?
                    this.bigDecimalValue().divide(dividend, this.getMathContext()) :
                    thisConv.inverse(
                            thisConv.convert(this.bigDecimalValue(), this.getMathContext())
                                    .divide(dividend, this.getMathContext()),
                            this.getMathContext()
                    );
        }

        return new DecimalQuantity<T>(result, this.getUnit(), isInterval);
    }

    @Nonnull
    @Override
    public DecimalQuantity<T> divide(double dividend) throws ArithmeticException {
        return this.divide(BigDecimal.valueOf(dividend));
    }

    @Nonnull
    @Override
    public DecimalQuantity<Dimensionless> divide(@Nonnull Quantity<T> dividend) throws ArithmeticException {
        BigDecimal dividendValue = dividend.bigDecimalValue();
        UnitConversion thisConv = this.getUnit().getConversion();
        UnitConversion thatConv = dividend.getUnit().getConversion();

        BigDecimal result;
        // do we still keep interval status or does it cancel out?
        // boolean isInterval;

        // Not derivable e.g. exp or log unit (dB, np)
        if (!(thisConv instanceof LinearConversion)) {
            BigDecimal thatResult = thatConv.convert(dividendValue, this.getMathContext());
            if (thatResult.compareTo(BigDecimal.ZERO) == 0) {
                throw new ArithmeticException("Divisor argument passed in has value 0. " +
                        "A number cannot be divided by 0.");
            }
            result = thisConv.convert(this.bigDecimalValue(), this.getMathContext())
                    .divide(thatResult, this.getMathContext());

        } else if (thisConv instanceof MultiplicativeConversion) {
            // we're multiplicative here
            if (dividendValue.compareTo(BigDecimal.ZERO) == 0) {
                throw new ArithmeticException("Divisor argument passed in has value 0. " +
                        "A number cannot be divided by 0.");
            }
            result = this.bigDecimalValue().divide(dividendValue, this.getMathContext());
        } else {
            // derivable but not strictly multiplicative unless is an interval e.g. temp units
            LinearConversion thisConvD = (LinearConversion) thisConv;
            LinearConversion thatConvD = (LinearConversion) thatConv;

            BigDecimal thisVal = (this.isInterval()) ?
                    thisConvD.getRationalCoefficient().multiply(BigDecRational.valueOf(this.bigDecimalValue()))
                            .bigDecimalValue(this.getMathContext()) :
                    thisConvD.convert(this.bigDecimalValue(), this.getMathContext());

            BigDecimal thatVal = (dividend.isInterval()) ?
                    thatConvD.getRationalCoefficient().multiply(BigDecRational.valueOf(this.bigDecimalValue()))
                            .bigDecimalValue(this.getMathContext()) :
                    thatConvD.convert(dividendValue, this.getMathContext());

            if (thatVal.compareTo(BigDecimal.ZERO) == 0) {
                throw new ArithmeticException("Divisor argument passed in has value 0. " +
                        "A number cannot be divided by 0.");
            }

            result = thisVal.divide(thatVal, this.getMathContext());
        }

        return new DecimalQuantity<Dimensionless>(result, ReferenceUnit.ONE);
    }

    @Nonnull
    @Override
    public DecimalQuantity<Dimensionless> relativeDifference(@Nonnull Quantity<T> that, @Nonnull FunctionXY function) throws ArithmeticException {
        BigDecimal referenceValue = that.to(this.getUnit()).bigDecimalValue();
        BigDecimal diff = this.bigDecimalValue().subtract(referenceValue, this.getMathContext());
        BigDecimal result;
        if (diff.compareTo(BigDecimal.ZERO) == 0) {
            result = BigDecimal.ZERO;
        } else {
            BigDecimal funcResult = function.invoke(this.bigDecimalValue(), referenceValue);
            if (funcResult.compareTo(BigDecimal.ZERO) == 0) {
                throw new ArithmeticException("Cannot divide by zero");
            }
            result = diff.divide(funcResult, this.getMathContext()).abs();
        }

        return new DecimalQuantity<Dimensionless>(result, ReferenceUnit.ONE, this.getMathContext());
    }

    @Nonnull
    @Override
    public DecimalQuantity<T> negate() {
        return new DecimalQuantity<T>(this.bigDecimalValue().negate(),
                this.getUnit(),
                this.isInterval(),
                this.getMathContext());
    }

    @Nonnull
    @Override
    public DecimalQuantity<T> inc() {
        return new DecimalQuantity<T>(
                this.bigDecimalValue().add(BigDecimal.ONE, this.getMathContext()),
                this.getUnit(),
                this.isInterval(),
                this.getMathContext());
    }

    @Nonnull
    @Override
    public DecimalQuantity<T> dec() {
        return new DecimalQuantity<T>(
                this.bigDecimalValue().subtract(BigDecimal.ONE, this.getMathContext()),
                this.getUnit(),
                this.isInterval(),
                this.getMathContext());
    }

    /**
     * Returns a quantity whose value is rounded to x decimal places by {@code roundingMode}
     *
     * @param decimalPlaces the number of decimal places to round to
     * @param roundingMode  the math context
     * @return the rounded {@code OperationalQuantity<T>}
     * @throws ArithmeticException the arithmetic exception
     * @see MathContext
     * @see java.math.BigDecimal#round(MathContext) java.math.BigDecimal#round(MathContext)
     */
    @Nonnull
    public DecimalQuantity<T> round(int decimalPlaces, @Nonnull RoundingMode roundingMode) throws ArithmeticException {
        if (decimalPlaces < 0) {
            throw new ArithmeticException("Cannot round to " + decimalPlaces + " decimal places");
        }
        return new DecimalQuantity<T>(this.bigDecimalValue().setScale(decimalPlaces, roundingMode),
                this.getUnit(),
                this.isInterval(),
                this.getMathContext());
    }

    @Nonnull
    @Override
    public DecimalQuantity<Dimensionless> relativeChange(@Nonnull Quantity<T> reference) throws ArithmeticException {
        Unit<T> rootUnit = this.getUnit().getReferenceUnit();
        BigDecimal thisValue = this.to(rootUnit).bigDecimalValue();
        BigDecimal referenceValue = reference.to(rootUnit).bigDecimalValue();

        BigDecimal result;

        if (thisValue.compareTo(referenceValue) == 0) {
            result = BigDecimal.ZERO;
        } else {
            if (referenceValue.compareTo(BigDecimal.ZERO) == 0) {
                throw new ArithmeticException("Cannot divide by zero");
            }
            result = thisValue.subtract(referenceValue, this.getMathContext())
                    .divide(referenceValue.abs(this.getMathContext()), this.getMathContext());
        }

        return new DecimalQuantity<Dimensionless>(result, ReferenceUnit.ONE);
    }


    @Nonnull
    @Override
    public DecimalQuantity<T> abs() {
        return new DecimalQuantity<T>(
                this.bigDecimalValue().abs(this.getMathContext()),
                this.getUnit(),
                this.isInterval(),
                this.getMathContext());
    }

    @Nonnull
    @Override
    public DecimalQuantity<T> setPrecision(@Nonnull MathContext mathContext) {
        return new DecimalQuantity<T>(
                this.bigDecimalValue().round(mathContext),
                this.getUnit(),
                this.isInterval(),
                this.getMathContext());
    }

    @Nonnull
    @Override
    public DecimalQuantity<T> toHuman() {
        if (!this.getUnit().canPrefix()) {
            return this;
        } if (this.getUnit() instanceof PrefixedUnit) {
            return this.to(((PrefixedUnit<T>) this.getUnit()).getPrefixedUnit()).toHuman();
        } else {
            MetricPrefix prefix = MetricPrefix.getPrefixFor(this.doubleValue());
            if (prefix == null) {
                return this;
            } else {
                return this.to(prefix.prefix(this.getUnit()));
            }
        }
    }
}
