/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;

import java.math.BigDecimal;
import java.math.MathContext;

import javax.annotation.Nonnull;

/**
 * <p>
 *     This interface represents a conversion which can be applied to a given number.
 *     Classes implementing this will typically be equivalent to a mathematical operand.
 * </p>
 *
 * @author  Tom Reader
 * @version 0.1.0
 * @see <a href="https://en.wikipedia.org/wiki/Operand">Wikipedia - Mathematical operands</a>
 * @since   0.1.0
 */
public interface UnitConversion {

    /**
     * Applies the conversion on the input using {@code double} operations.
     * This is less precise and may cause precision loss - but should be faster.
     *
     * @param input the input to be applied by this
     * @return the result after the conversion is applied to the input
     * @see <a href="https://en.wikipedia.org/wiki/Floating-point_arithmetic#Accuracy_problems">
     *     Floating-point arithmetic: Accuracy problems</a>
     */
    public double convert(double input);

    /**
     * Applies the inverse conversion on the input using {@code double} operations.
     * This is less precise and may cause precision loss - but should be faster.
     *
     * @param input the input to be applied by this
     * @return the result after the inverse conversion is applied to the input
     * @see <a href="https://en.wikipedia.org/wiki/Floating-point_arithmetic#Accuracy_problems">
     *     Floating-point arithmetic: Accuracy problems</a>
     */
    public double inverse(double input);

    /**
     * Applies the conversion on the input using BigDecimal operations.
     *
     * @param input the input to be applied by this
     * @param mathContext the {@code MathContext} to use for the operation
     * @return the result after the conversion is applied to the input
     * @see BigDecimal
     */
    @Nonnull
    public BigDecimal convert(@Nonnull BigDecimal input, MathContext mathContext);

    /**
     * Applies the inverse conversion on the input using BigDecimal operations.
     *
     * @param input the input to be applied by this
     * @param mathContext the {@code MathContext} to use for the operation
     * @return the result after the inverse conversion is applied to the input
     * @see BigDecimal
     */
    @Nonnull
    public BigDecimal inverse(@Nonnull BigDecimal input, MathContext mathContext);

//    /**
//     * Returns a conversion which is the inverse of this converter.
//     * Where x is a number and conversion is a {@code Conversion}:
//     * {@code x ~= conversion.invert().apply(conversion.apply(x))}
//     *
//     * @return the conversion which represents the inverse of this
//     * @see https://en.wikipedia.org/wiki/Multiplicative_inverse
//     */
//    @Nonnull
//    public UnitConversion invert();
}
