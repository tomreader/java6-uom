/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.quantity;

import com.gitlab.thomasreader.uom.conversion.LinearConversion;
import com.gitlab.thomasreader.uom.conversion.MultiplicativeConversion;
import com.gitlab.thomasreader.uom.conversion.UnitConversion;
import com.gitlab.thomasreader.uom.quantity.type.Dimensionless;
import com.gitlab.thomasreader.uom.math.DoubleMath;
import com.gitlab.thomasreader.uom.math.FunctionXY;
import com.gitlab.thomasreader.uom.unit.PrefixedUnit;
import com.gitlab.thomasreader.uom.unit.ReferenceUnit;
import com.gitlab.thomasreader.uom.unit.Unit;
import com.gitlab.thomasreader.uom.unit.prefix.MetricPrefix;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.NumberFormat;

import javax.annotation.Nonnull;

/**
 * Double implementation of {@link Quantity<T>}.
 *
 * @param <T> the type of quantity
 * @author Tom Reader
 * @version 0.1.0
 * @see Quantity
 * @see <a href="https://en.wikipedia.org/wiki/Quantity">Wikipedia - Quantity</a>
 * @see <a href="http://www.dsc.ufcg.edu.br/~jacques/cursos/map/recursos/fowler-ap/Analysis%20Pattern%20Quantity.htm">     Martin Fowler - Quantity</a>
 * @since 0.1.0
 */
public class DoubleQuantity<T extends Quantity<T>> extends AbstractQuantity<T> {

    private final double value;

    /**
     * Instantiates a new Double quantity.
     *
     * @param value      the value
     * @param unit       the unit
     * @param isInterval whether this should resemble an interval
     */
    public DoubleQuantity(double value, @Nonnull Unit<T> unit, boolean isInterval) {
        super(unit, isInterval);
        this.value = value;
    }

    /**
     * Instantiates a new Double quantity.
     *
     * @param value the magnitude
     * @param unit  the unit
     */
    public DoubleQuantity(double value, @Nonnull Unit<T> unit) {
        this(value, unit, false);
    }

    @Override
    public double doubleValue() {
        return this.value;
    }

    @Nonnull
    @Override
    public Double numberValue() {
        return this.doubleValue();
    }

    @Nonnull
    @Override
    public BigDecimal bigDecimalValue() {
        return BigDecimal.valueOf(this.value);
    }

    @Nonnull
    @Override
    public String getValue(@Nonnull NumberFormat numberFormatter) {
        return numberFormatter.format(this.doubleValue());
    }

    @Nonnull
    @Override
    public DoubleQuantity<T> to(@Nonnull Unit<T> target) {
        if (target.equals(this.getUnit())) {
            return this;
        }

        double result;
        boolean resultIsInterval = this.isInterval();

        if (this.getUnit().getConversion().equals(target.getConversion())) {
            result = this.doubleValue();

        } else if (this.isInterval() && this.getUnit().getConversion() instanceof LinearConversion &&
                !(this.getUnit().getConversion() instanceof MultiplicativeConversion)) {
            double tempResult = ((LinearConversion) this.getUnit().getConversion()).getCoefficient()
                    * this.doubleValue();

            if (target.getConversion() instanceof LinearConversion) {
                tempResult = tempResult / ((LinearConversion) target.getConversion()).getCoefficient();
            } else {
                // do we throw here or just convert?
                tempResult = target.getConversion().inverse(tempResult);
                resultIsInterval = false;
            }
            result = tempResult;

        } else {
            result = target.getConversion().inverse(
                    this.getUnit().getConversion().convert(this.doubleValue()));
        }

        return new DoubleQuantity<T>(
                result,
                target,
                resultIsInterval
        );
    }

    @Override
    public int compareTo(@Nonnull Quantity<T> otherQuantity) {
        Quantity<T> otherQuantityConverted = otherQuantity.to(this.getUnit());
        return Double.compare(this.doubleValue(), otherQuantityConverted.doubleValue());
    }

    @Override
    public int compareTo(@Nonnull Quantity<T> that, double epsilon) {
        Quantity<T> convertedThat = that.to(this.getUnit());
        double thatValue = convertedThat.doubleValue();
        if (this.doubleValue() == thatValue) {
            return 0;
        }
        double relativeDiff = this.relativeDifference(convertedThat, FunctionXY.MAX_OF_ABSOLUTES).doubleValue();
        if (relativeDiff <= epsilon) {
            return 0;
        } else {
            return Double.compare(this.doubleValue(), thatValue);
        }
    }

    @Nonnull
    @Override
    public DoubleQuantity<T> add(@Nonnull Quantity<T> augend) {
        // add not same as subtract.negate() -> 10dB + 10dB ~= 13dB, 10dB - (-10dB) ~= 10.4dB
        // negative logarithmic units model just very small absolute numbers.
        // -> therefore work out result of these units or subtract.negate()
        UnitConversion thisConv = this.getUnit().getConversion();
        UnitConversion thatConv = augend.getUnit().getConversion();

        double result;
        boolean isInterval = this.isInterval() && augend.isInterval();

        if (!(thisConv instanceof LinearConversion) ||
                !(augend.getUnit().getConversion() instanceof LinearConversion)) {

            result = thisConv.convert(this.doubleValue()) +
                    thatConv.convert(augend.doubleValue());
            result = thisConv.inverse(result);
            isInterval = false;
        } else if (thisConv instanceof MultiplicativeConversion && thatConv instanceof MultiplicativeConversion) {
            // we're multiplicative here
            result = this.doubleValue() + augend.to(this.getUnit()).doubleValue();
        } else {
            // derivable but not strictly multiplicative unless is an interval e.g. temp units
            LinearConversion thisConvD = (LinearConversion) thisConv;
            LinearConversion thatConvD = (LinearConversion) thatConv;

            double thisVal = (this.isInterval()) ?
                    thisConvD.getCoefficient() * this.doubleValue() :
                    thisConvD.convert(this.doubleValue());

            double thatVal = (augend.isInterval()) ?
                    thatConvD.getCoefficient() * augend.doubleValue() :
                    thatConvD.convert(augend.doubleValue());


            result = thisConv.inverse(thisVal + thatVal);
        }


        return new DoubleQuantity<T>(result, this.getUnit(), isInterval);
    }

    @Nonnull
    private DoubleQuantity<T> subtract(@Nonnull Quantity<T> subtrahend, boolean forceInterval) {
        UnitConversion thisConv = this.getUnit().getConversion();
        UnitConversion thatConv = subtrahend.getUnit().getConversion();

        double result;
        boolean isInterval = forceInterval || (this.isInterval() && subtrahend.isInterval());

        // Not derivable e.g. exp or log unit (dB, np)
        if (!(thisConv instanceof LinearConversion) || !(thatConv instanceof LinearConversion)) {
            result = thisConv.convert(this.doubleValue()) - thatConv.convert(subtrahend.doubleValue());
            result = thisConv.inverse(result);
            // cannot force a non-linear unit to become an interval?
            isInterval = false;
        } else if (thisConv instanceof MultiplicativeConversion && thatConv instanceof MultiplicativeConversion) {
            // we're multiplicative here
            result = this.doubleValue() - subtrahend.to(this.getUnit()).doubleValue();
        } else {
            // derivable but not strictly multiplicative unless is an interval e.g. temp units
            LinearConversion thisConvD = (LinearConversion) thisConv;
            LinearConversion thatConvD = (LinearConversion) thatConv;

            double thisVal = (this.isInterval()) ?
                    thisConvD.getCoefficient() * this.doubleValue() :
                    thisConvD.convert(this.doubleValue());

            double thatVal = (subtrahend.isInterval()) ?
                    thatConvD.getCoefficient() * subtrahend.doubleValue() :
                    thatConvD.convert(subtrahend.doubleValue());


            result = thisConv.inverse(thisVal - thatVal);
        }

        return new DoubleQuantity<T>(result, this.getUnit(), isInterval);
    }

    @Nonnull
    @Override
    public DoubleQuantity<T> subtract(@Nonnull Quantity<T> subtrahend) {
        return this.subtract(subtrahend, false);
    }

    @Nonnull
    @Override
    public DoubleQuantity<T> interval(@Nonnull Quantity<T> subtrahend) {
        return this.subtract(subtrahend, true);
    }

    @Nonnull
    @Override
    public DoubleQuantity<T> multiply(double multiplicand) {
        UnitConversion thisConv = this.getUnit().getConversion();

        double result;
        boolean isInterval = this.isInterval();

        // Not derivable e.g. exp or log unit (dB, np)
        if (!(thisConv instanceof LinearConversion)) {
            result = thisConv.convert(this.doubleValue()) * multiplicand;
            result = thisConv.inverse(result);
            // cannot force a non-linear unit to become an interval?
            isInterval = false;
        } else if (thisConv instanceof MultiplicativeConversion) {
            // we're multiplicative here
            result = this.doubleValue() * multiplicand;
        } else {
            // derivable but not strictly multiplicative unless is an interval e.g. temp units
            LinearConversion thisConvD = (LinearConversion) thisConv;
            result = (this.isInterval()) ?
                    this.doubleValue() * multiplicand :
                    thisConv.inverse(thisConv.convert(this.doubleValue()) * multiplicand);
        }

        return new DoubleQuantity<T>(result, this.getUnit(), isInterval);
    }

    @Nonnull
    @Override
    public DoubleQuantity<T> multiply(@Nonnull BigDecimal multiplicand) {
        return this.multiply(multiplicand.doubleValue());
    }

    @Nonnull
    @Override
    public DoubleQuantity<T> divide(double dividend) throws ArithmeticException {
        if (dividend == 0d || dividend == -0d) {
            throw new ArithmeticException("Divisor argument passed in has value 0. " +
                    "A number cannot be divided by 0.");
        }
        UnitConversion thisConv = this.getUnit().getConversion();

        double result;
        boolean isInterval = this.isInterval();

        // Not derivable e.g. exp or log unit (dB, np)
        if (!(thisConv instanceof LinearConversion)) {
            result = thisConv.convert(this.doubleValue()) / dividend;
            result = thisConv.inverse(result);
            // cannot force a non-linear unit to become an interval?
            isInterval = false;
        } else if (thisConv instanceof MultiplicativeConversion) {
            // we're multiplicative here
            result = this.doubleValue() / dividend;
        } else {
            // derivable but not strictly multiplicative unless is an interval e.g. temp units
            LinearConversion thisConvD = (LinearConversion) thisConv;
            result = (this.isInterval()) ?
                    this.doubleValue() / dividend :
                    thisConv.inverse(thisConv.convert(this.doubleValue()) / dividend);
        }

        return new DoubleQuantity<T>(result, this.getUnit(), isInterval);
    }

    @Nonnull
    @Override
    public DoubleQuantity<T> divide(@Nonnull BigDecimal dividend) throws ArithmeticException {
        return this.divide(dividend.doubleValue());
    }

    @Nonnull
    @Override
    public DoubleQuantity<Dimensionless> divide(@Nonnull Quantity<T> dividend) throws ArithmeticException {
        double dividendValue = dividend.doubleValue();
        UnitConversion thisConv = this.getUnit().getConversion();
        UnitConversion thatConv = dividend.getUnit().getConversion();

        double result;
        // do we still keep interval status or does it cancel out?
        // boolean isInterval;

        // Not derivable e.g. exp or log unit (dB, np)
        if (!(thisConv instanceof LinearConversion)) {
            double thatResult = thatConv.convert(dividendValue);
            if (thatResult == 0d || thatResult == -0d) {
                throw new ArithmeticException("Divisor argument passed in has value 0. " +
                        "A number cannot be divided by 0.");
            }
            result = thisConv.convert(this.doubleValue()) / thatResult;
        } else if (thisConv instanceof MultiplicativeConversion) {
            // we're multiplicative here
            if (dividendValue == 0d || dividendValue == -0d) {
                throw new ArithmeticException("Divisor argument passed in has value 0. " +
                        "A number cannot be divided by 0.");
            }
            result = this.doubleValue() / dividendValue;
        } else {
            // derivable but not strictly multiplicative unless is an interval e.g. temp units
            LinearConversion thisConvD = (LinearConversion) thisConv;
            LinearConversion thatConvD = (LinearConversion) thatConv;

            double thisVal = (this.isInterval()) ?
                    thisConvD.getCoefficient() * this.doubleValue() : thisConvD.convert(this.doubleValue());
            double thatVal = (dividend.isInterval()) ?
                    thatConvD.getCoefficient() * dividendValue : thatConvD.convert(dividendValue);

            if (thatVal == 0d || thatVal == -0d) {
                throw new ArithmeticException("Divisor argument passed in has value 0. " +
                        "A number cannot be divided by 0.");
            }

            result = thisVal / thatVal;
        }

        return new DoubleQuantity<Dimensionless>(result, ReferenceUnit.ONE);
    }

    @Nonnull
    @Override
    public DoubleQuantity<T> negate() {
        return new DoubleQuantity<T>(this.doubleValue() * -1, this.getUnit(), this.isInterval());
    }

    @Nonnull
    @Override
    public DoubleQuantity<T> inc() {
        return new DoubleQuantity<T>(this.doubleValue() + 1, this.getUnit(), this.isInterval());
    }

    @Nonnull
    @Override
    public DoubleQuantity<T> dec() {
        return new DoubleQuantity<T>(this.doubleValue() - 1, this.getUnit(), this.isInterval());
    }

    /**
     * Round this quantity using {@link Math#rint(double)} when the distance between the rint value
     * and this value is smaller than the epsilon.
     *
     * @param epsilon the epsilon
     * @return the double quantity
     * @see <a href="https://en.wikipedia.org/wiki/Machine_epsilon">Wikipedia - Machine epsilon</a>
     */
    @Nonnull
    public DoubleQuantity<T> rint(double epsilon) {
        double rint = Math.rint(this.value);
//        DoubleQuantity<T> rintQ = this.getUnit().of(rint);
//        double relDiff = this.relativeDifference(this.getUnit().of(rint),
//                RelativeFunctions.MAX_OF_ABSOLUTES).doubleValue();
//        return (relDiff < Math.abs(epsilon)) ? rintQ : this;

        return (Math.abs(this.value - rint) > Math.abs(epsilon)) ? this :
            new DoubleQuantity<T>(rint, this.getUnit(), this.isInterval());
    }

    @Nonnull
    @Override
    public DoubleQuantity<Dimensionless> relativeDifference(@Nonnull Quantity<T> that, @Nonnull FunctionXY function) throws ArithmeticException {
        double referenceValue = that.to(this.getUnit()).doubleValue();
        double diff = this.doubleValue() - referenceValue;
        double result;
        if (diff == 0 || diff == -0) {
            result = 0;
        } else {
            double funcResult = function.invoke(this.doubleValue(), referenceValue);
            if (funcResult == 0) {
                throw new ArithmeticException("Cannot divide by zero");
            }
            result = Math.abs(diff / funcResult);
        }

        return new DoubleQuantity<Dimensionless>(result, ReferenceUnit.ONE);
    }

    @Nonnull
    @Override
    public DoubleQuantity<Dimensionless> relativeChange(@Nonnull Quantity<T> reference) throws ArithmeticException {
        Unit<T> rootUnit = this.getUnit().getReferenceUnit();
        double thisValue = this.to(rootUnit).doubleValue();
        double referenceValue = reference.to(rootUnit).doubleValue();

        double result;

        if (thisValue == referenceValue) {
            result = 0;
        } else {
            if (referenceValue == 0) {
                throw new ArithmeticException("Cannot divide by zero");
            }
            result = (thisValue - referenceValue) / Math.abs(referenceValue);
        }

        return new DoubleQuantity<Dimensionless>(result, ReferenceUnit.ONE);
    }

    @Nonnull
    @Override
    public DoubleQuantity<T> abs() {
        return new DoubleQuantity<T>(Math.abs(this.value), this.getUnit(), this.isInterval());
    }

    @Nonnull
    @Override
    public DoubleQuantity<T> setPrecision(@Nonnull MathContext mathContext) {
        // using double math performs the same arithmetic in about 30 % the time but loses in
        // accuracy ~20-25 % of the time
        return new DoubleQuantity<T>(
                DoubleMath.setPrecision(this.value, mathContext),
                this.getUnit(),
                this.isInterval()
        );
    }

    @Nonnull
    @Override
    public DoubleQuantity<T> toHuman() {
        if (!this.getUnit().canPrefix()) {
            return this;
        } if (this.getUnit() instanceof PrefixedUnit) {
            return this.to(((PrefixedUnit<T>) this.getUnit()).getPrefixedUnit()).toHuman();
        } else {
            MetricPrefix prefix = MetricPrefix.getPrefixFor(this.value);
            if (prefix == null) {
                return this;
            } else {
                return this.to(prefix.prefix(this.getUnit()));
            }
        }
    }
//    @Override
//    public <U extends Quantity<U>> Quantity<U> reciprocal(Unit<U> to) {
//        Quantity<T> baseT = this.to(this.getUnit().getRootUnit());
//        double reciprocalValue = 1.0 / baseT.doubleValue();
//        return new DoubleQuantity<U>(to.getConversion().invertedConvert(reciprocalValue), to);
//    }
}
