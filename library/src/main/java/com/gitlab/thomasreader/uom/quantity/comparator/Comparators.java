/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.quantity.comparator;

import com.gitlab.thomasreader.uom.quantity.Quantity;

import java.util.Comparator;


/**
 * Quantity comparators.
 * @author Tom Reader
 * @version 0.1.0
 * @since 0.1.0
 */
public abstract class Comparators {
    /**
     * Comparator to sort quantities in ascending order.
     *
     * @param <T> tthe quantity type
     * @return the comparator
     */
    public static <T extends Quantity<T>> Comparator<Quantity<T>> asc() {
        return new Comparator<Quantity<T>>() {
            @Override
            public int compare(Quantity<T> q1, Quantity<T> q2) {
                return q1.compareTo(q2);
            }
        };
    }

    /**
     * Comparator to sort quantities in descending order.
     *
     * @param <T> the quantity type
     * @return the comparator
     */
    public static <T extends Quantity<T>> Comparator<Quantity<T>> desc() {
        return new Comparator<Quantity<T>>() {
            @Override
            public int compare(Quantity<T> q1, Quantity<T> q2) {
                return -(q1.compareTo(q2));
            }
        };
    }
}
