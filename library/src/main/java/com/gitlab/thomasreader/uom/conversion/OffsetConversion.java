/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;

import com.gitlab.thomasreader.uom.math.BigDecRational;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Objects;

import javax.annotation.Nonnull;

/**
 * An offset conversion i.e. {@code x = y + offset}
 *
 * @author Tom Reader
 * @version 0.1.0
 * @since 0.1.0
 */
public abstract class OffsetConversion implements LinearConversion {

    @Override
    public double getCoefficient() {
        return 1;
    }

    @Nonnull
    @Override
    public BigDecRational getRationalCoefficient() {
        return BigDecRational.ONE;
    }

    /**
     * Gets the offset.
     *
     * @return the offset
     */
    public abstract double getOffset();

    /**
     * Gets the offset.
     *
     * @return the offset
     */
    @Nonnull
    public abstract BigDecimal getDecimalOffset();

    /**
     * Adds the offset to the given input using {@code double} precision - faster
     * than {@link #convert} but less precise.
     *
     * @param input the inputted number
     * @return number after the input has been added by this offset
     * @since 0.2.0
     */
    @Override
    public double convert(double input) {
        return input + this.getOffset();
    }

    /**
     * Subtracts the offset to the given input using {@code double} precision - faster
     * than {@link #convert} but less precise.
     *
     * @param input the inputted number
     * @return number after the input has been added by this offset
     * @since 0.2.0
     */
    @Override
    public double inverse(double input) {
        return input - this.getOffset();
    }

    /**
     * Adds the conversion offset to the given input using {@code BigDecimal} precision.
     *
     * @param input the inputted number
     * @param mathContext the {@code MathContext} to use for the operation
     * @return number after the input has been added to by this offset
     */
    @Nonnull
    @Override
    public BigDecimal convert(@Nonnull BigDecimal input, MathContext mathContext) {
        return input.add(this.getDecimalOffset(), mathContext);
    }

    /**
     * Subtracts the offset from the given input using {@code BigDecimal} precision.
     *
     * @param input the inputted number
     * @param mathContext the {@code MathContext} to use for the operation
     * @return number after the input has been added to by this offset
     */
    @Nonnull
    @Override
    public BigDecimal inverse(@Nonnull BigDecimal input, MathContext mathContext) {
        return input.subtract(this.getDecimalOffset(), mathContext);
    }

    /**
     *
     * @return {@code +factor} if the factor is positive otherwise {@code -factor}
     */
    @Nonnull
    @Override
    public abstract String toString();

    @Override
    public abstract int hashCode();

    @Override
    public abstract boolean equals(Object o);

    /**
     * An offset conversion using a double to model the offset.
     * @author Tom Reader
     * @version 0.1.0
     * @since 0.1.0
     */
    public static class DoubleOffset extends OffsetConversion {
        private final double offset;

        /**
         * Instantiates a new Double offset.
         *
         * @param offset the offset
         */
        public DoubleOffset(double offset) {
            this.offset = offset;
        }

        @Override
        public double getOffset() {
            return this.offset;
        }

        @Nonnull
        @Override
        public BigDecimal getDecimalOffset() {
            return BigDecimal.valueOf(this.getOffset());
        }

        @Nonnull
        @Override
        public String toString() {
            return (this.offset > 0) ? "+" + this.offset : String.valueOf(this.offset);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DoubleOffset that = (DoubleOffset) o;
            return Double.compare(that.getOffset(), getOffset()) == 0;
        }

        @Override
        public int hashCode() {
            return Objects.hash(getOffset());
        }
    }

    /**
     * An offset conversion using a BigDecimal to model the offset.
     * @author Tom Reader
     * @version 0.1.0
     * @since 0.1.0
     */
    public static class DecimalOffset extends OffsetConversion {
        @Nonnull
        private final BigDecimal decimalOffset;
        private double cachedOffset = 0;
        private boolean cached = false;

        /**
         * Instantiates a new Decimal offset.
         *
         * @param decimalOffset the decimal offset
         */
        public DecimalOffset(@Nonnull BigDecimal decimalOffset) {
            this.decimalOffset = decimalOffset;
        }

        @Override
        public double getOffset() {
            if (!cached) {
                this.cachedOffset = this.getDecimalOffset().doubleValue();
                cached = true;
            }
            return this.cachedOffset;
        }

        @Nonnull
        @Override
        public BigDecimal getDecimalOffset() {
            return this.decimalOffset;
        }

        @Nonnull
        @Override
        public String toString() {
            return (this.decimalOffset.signum() >= 0) ? "+" + this.decimalOffset : this.decimalOffset.toString();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DecimalOffset that = (DecimalOffset) o;
            return getDecimalOffset().equals(that.getDecimalOffset());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getDecimalOffset());
        }
    }
}
