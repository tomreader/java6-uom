/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.quantity;

import com.gitlab.thomasreader.uom.quantity.comparator.Comparators;
import com.gitlab.thomasreader.uom.unit.Unit;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


/**
 * Useful commands for arrays and collections of quantities.
 *
 * @author Tom Reader
 * @version 0.1.0
 * @since   0.1.0
 */
public class Quantities {
    private Quantities() {
    }

    /**
     * Sorts the list in ascending order.
     *
     * @param <T>        the quantity type
     * @param quantities the quantities
     * @return the sorted list
     */
    public static <T extends Quantity<T>> List<Quantity<T>> sortAsc(List<Quantity<T>> quantities) {
        Collections.sort(quantities, Comparators.<T>asc());
        return quantities;
    }

    /**
     * Sorts the list in descending order.
     *
     * @param <T>        the quantity type
     * @param quantities the quantities
     * @return the sorted list
     */
    public static <T extends Quantity<T>> List<Quantity<T>> sortDesc(List<Quantity<T>> quantities) {
        Collections.sort(quantities, Comparators.<T>desc());
        return quantities;
    }

    /**
     * Sorts the array in ascending order.
     *
     * @param <T>        the quantity type
     * @param quantities the quantities
     * @return the sorted array
     */
    public static <T extends Quantity<T>> Quantity<T>[] sortAsc(Quantity<T>[] quantities) {
        Arrays.sort(quantities, Comparators.<T>asc());
        return quantities;
    }

    /**
     * Sorts the array in descending order.
     *
     * @param <T>        the quantity type
     * @param quantities the quantities
     * @return the sorted array
     */
    public static <T extends Quantity<T>> Quantity<T>[] sortDesc(Quantity<T>[] quantities) {
        Arrays.sort(quantities, Comparators.<T>desc());
        return quantities;
    }

    /**
     * Returns the quantity of the smallest value.
     *
     * @param <T>        the quantity type
     * @param quantities the quantities
     * @return the smallest quantity
     */
    public static <T extends Quantity<T>> Quantity<T> min(Collection<Quantity<T>> quantities) {
        if (quantities.size() > 0) {
            Quantity<T> smallestQuantity = null;
            for (Quantity<T> quantity : quantities) {
                if (smallestQuantity == null) {
                    smallestQuantity = quantity;
                } else {
                    if (quantity.compareTo(smallestQuantity) < 0) {
                        smallestQuantity = quantity;
                    }
                }
            }
            return smallestQuantity;
        }
        return null;
    }

    /**
     * Returns the quantity of the largest value.
     *
     * @param <T>        the quantity type
     * @param quantities the quantities
     * @return the largest quantity
     */
    public static <T extends Quantity<T>> Quantity<T> max(Collection<Quantity<T>> quantities) {
        if (quantities.size() > 0) {
            Quantity<T> largestQuantity = null;
            for (Quantity<T> quantity : quantities) {
                if (largestQuantity == null) {
                    largestQuantity = quantity;
                } else {
                    if (quantity.compareTo(largestQuantity) > 0) {
                        largestQuantity = quantity;
                    }
                }
            }
            return largestQuantity;
        }
        return null;
    }

    /**
     * Returns a quantity modelling the mean value of the quantities.
     *
     * @param <T>        the quantity type
     * @param quantities the quantities
     * @return the mean quantity
     */
    public static <T extends Quantity<T>> Quantity<T> mean(Collection<Quantity<T>> quantities) {
        if (quantities.size() > 0) {
            double accumulator = 0;
            Unit<T> unit = null;
            for (Quantity<T> quantity : quantities) {
                if (unit == null) {
                    unit = quantity.getUnit();
                }
                accumulator += quantity.to(unit).doubleValue();
            }
            return new DoubleQuantity<T>(accumulator / quantities.size(), unit);
        }
        return null;
    }

    /**
     * Returns a {@link DecimalQuantity} modelling the mean value of the quantities.
     *
     * @param <T>        the quantity type
     * @param quantities the quantities
     * @return the mean quantity
     */
    public static <T extends Quantity<T>> Quantity<T> decimalMean(Collection<Quantity<T>> quantities) {
        if (quantities.size() > 0) {
            BigDecimal accumulator = BigDecimal.ZERO;
            Unit<T> unit = null;
            for (Quantity<T> quantity : quantities) {
                if (unit == null) {
                    unit = quantity.getUnit();
                }
                accumulator = accumulator.add(quantity.to(unit).bigDecimalValue());
            }
            return new DecimalQuantity<T>(
                    accumulator.divide(BigDecimal.valueOf(quantities.size()), MathContext.DECIMAL128),
                    unit
            );
        }
        return null;
    }

    /**
     * Returns a quantity modelling the median value of the quantities.
     *
     * @param <T>        the quantity type
     * @param quantities the quantities
     * @return the median quantity
     */
    public static <T extends Quantity<T>> Quantity<T> median(Collection<Quantity<T>> quantities) {
        if (quantities.size() > 0) {
            List<Quantity<T>> quantityList = new ArrayList<Quantity<T>>(quantities);
            int length = quantityList.size();
            Collections.sort(quantityList);
            if (length % 2 == 0) {
                int index1 = (length - 1) / 2;
                int index2 = (length) / 2;
                return (quantityList.get(index1).add(quantityList.get(index2))).divide(2);
            } else {
                return quantityList.get((length - 1) / 2);
            }
        }
        return null;
    }

    /**
     * Returns the quantity of the smallest value.
     *
     * @param <T>        the quantity type
     * @param quantities the quantities
     * @return the smallest quantity
     */
    public static <T extends Quantity<T>> Quantity<T> min(Quantity<T>[] quantities) {
        if (quantities.length > 0) {
            Quantity<T> smallestQuantity = null;
            for (Quantity<T> quantity : quantities) {
                if (smallestQuantity == null) {
                    smallestQuantity = quantity;
                } else {
                    if (quantity.compareTo(smallestQuantity) < 0) {
                        smallestQuantity = quantity;
                    }
                }
            }
            return smallestQuantity;
        }
        return null;
    }

    /**
     * Returns the quantity of the largest value.
     *
     * @param <T>        the quantity type
     * @param quantities the quantities
     * @return the largest quantity
     */
    public static <T extends Quantity<T>> Quantity<T> max(Quantity<T>[] quantities) {
        if (quantities.length > 0) {
            Quantity<T> largestQuantity = null;
            for (Quantity<T> quantity : quantities) {
                if (largestQuantity == null) {
                    largestQuantity = quantity;
                } else {
                    if (quantity.compareTo(largestQuantity) > 0) {
                        largestQuantity = quantity;
                    }
                }
            }
            return largestQuantity;
        }
        return null;
    }

    /**
     * Returns a quantity modelling the mean value of the quantities.
     *
     * @param <T>        the quantity type
     * @param quantities the quantities
     * @return the mean quantity
     */
    public static <T extends Quantity<T>> Quantity<T> mean(Quantity<T>[] quantities) {
        if (quantities.length > 0) {
            double accumulator = 0;
            Unit<T> unit = null;
            for (Quantity<T> quantity : quantities) {
                if (unit == null) {
                    unit = quantity.getUnit();
                }
                accumulator += quantity.to(unit).doubleValue();
            }
            return new DoubleQuantity<T>(accumulator / quantities.length, unit);
        }
        return null;
    }

    /**
     * Returns a {@link DecimalQuantity} modelling the mean value of the quantities.
     *
     * @param <T>        the quantity type
     * @param quantities the quantities
     * @return the mean quantity
     */
    public static <T extends Quantity<T>> Quantity<T> decimalMean(Quantity<T>[]  quantities) {
        if (quantities.length > 0) {
            BigDecimal accumulator = BigDecimal.ZERO;
            Unit<T> unit = null;
            for (Quantity<T> quantity : quantities) {
                if (unit == null) {
                    unit = quantity.getUnit();
                }
                accumulator = accumulator.add(quantity.to(unit).bigDecimalValue());
            }
            return new DecimalQuantity<T>(
                    accumulator.divide(BigDecimal.valueOf(quantities.length), MathContext.DECIMAL128),
                    unit
            );
        }
        return null;
    }

    /**
     * Returns a quantity modelling the median value of the quantities.
     *
     * @param <T>        the quantity type
     * @param quantities the quantities
     * @return the median quantity
     */
    public static <T extends Quantity<T>> Quantity<T> median(Quantity<T>[] quantities) {
        if (quantities.length > 0) {
            int length = quantities.length;
            Quantity<T>[] quantityList = Arrays.copyOf(quantities, length);
            Arrays.sort(quantityList);
            if (length % 2 == 0) {
                int index1 = (length - 1) / 2;
                int index2 = (length) / 2;
                return (quantityList[index1].add(quantityList[index2])).divide(2);
            } else {
                return quantityList[(length - 1) / 2];
            }
        }
        return null;
    }
}
