/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;

import com.gitlab.thomasreader.uom.math.BigDecRational;

import java.math.BigDecimal;
import java.math.MathContext;

import javax.annotation.Nonnull;

/**
 * Linear conversion using composite conversions for its coefficient and offset. The conversion
 * resembles a function of: {@code ax+c} or {@code (x+c)*a} where a is the coefficient
 * and c is the constant.
 * @author Tom Reader
 * @version 0.1.0
 * @since 0.1.0
 */
public class LinearOffsetConversion implements LinearConversion {

    @Nonnull
    private final MultiplicativeConversion coefficient;
    @Nonnull
    private final OffsetConversion offset;
    private final boolean coefficientFirst;

    LinearOffsetConversion(@Nonnull MultiplicativeConversion coefficient, @Nonnull OffsetConversion offset, boolean coefficientFirst) {
        this.coefficient = coefficient;
        this.offset = offset;
        this.coefficientFirst = coefficientFirst;
    }

    /**
     * Creates a linear conversion of the form {@code ax+c}.
     *
     * @param multiplicativeConversion the multiplicative coefficient conversion
     * @param offsetConversion         the offset conversion
     * @return the linear conversion
     */
    public static LinearOffsetConversion coefficientThenOffset(
            @Nonnull MultiplicativeConversion multiplicativeConversion, @Nonnull OffsetConversion offsetConversion) {

        return new LinearOffsetConversion(multiplicativeConversion, offsetConversion, true);
    }

    /**
     * Creates a linear conversion of the form {@code (x+c)*a}.
     *
     * @param offsetConversion         the offset conversion
     * @param multiplicativeConversion the multiplicative coefficient conversion
     * @return the linear conversion
     */
    public static LinearOffsetConversion offsetThenCoefficient(
            @Nonnull OffsetConversion offsetConversion, @Nonnull MultiplicativeConversion multiplicativeConversion) {

        return new LinearOffsetConversion(multiplicativeConversion, offsetConversion, false);
    }

    @Override
    public double getCoefficient() {
        return this.coefficient.getCoefficient();
    }

    @Nonnull
    @Override
    public BigDecRational getRationalCoefficient() {
        return this.coefficient.getRationalCoefficient();
    }

    @Override
    public double convert(double input) {
        return (coefficientFirst) ? offset.convert(coefficient.convert(input)) : coefficient.convert(offset.convert(input));
    }

    @Override
    public double inverse(double input) {
        return (!coefficientFirst) ? offset.inverse(coefficient.inverse(input)) : coefficient.inverse(offset.inverse(input));
    }

    @Nonnull
    @Override
    public BigDecimal convert(@Nonnull BigDecimal input, MathContext mathContext) {
        return (coefficientFirst) ? offset.convert(coefficient.convert(input, mathContext), mathContext) : coefficient.convert(offset.convert(input, mathContext), mathContext);
    }

    @Nonnull
    @Override
    public BigDecimal inverse(@Nonnull BigDecimal input, MathContext mathContext) {
        return (!coefficientFirst) ? offset.inverse(coefficient.inverse(input, mathContext), mathContext) : coefficient.inverse(offset.inverse(input, mathContext), mathContext);
    }

    @Override
    public String toString() {
        return (coefficientFirst) ? this.coefficient.toString() + " -> " + this.offset.toString() :
                this.offset.toString() + " -> " + this.coefficient.toString();
    }
}
