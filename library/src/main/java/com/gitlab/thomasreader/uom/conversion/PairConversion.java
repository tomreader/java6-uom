/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;

import java.math.BigDecimal;
import java.math.MathContext;

import javax.annotation.Nonnull;

/**
 * Composite conversion consisting of two {@link UnitConversion}
 * @author Tom Reader
 * @version 0.1.0
 * @since 0.1.0
 */
public class PairConversion implements UnitConversion {

    @Nonnull
    private final UnitConversion conversion1;
    @Nonnull
    private final UnitConversion conversion2;

    /**
     * Instantiates a new Pair conversion.
     *
     * @param conversion1 the first conversion
     * @param conversion2 the second conversion
     */
    public PairConversion(@Nonnull UnitConversion conversion1, @Nonnull UnitConversion conversion2) {
        this.conversion1 = conversion1;
        this.conversion2 = conversion2;
    }

    /**
     * Gets the first conversion.
     *
     * @return the first conversion
     */
    @Nonnull
    public UnitConversion getConversion1() {
        return this.conversion1;
    }

    /**
     * Gets the second conversion.
     *
     * @return the second conversion
     */
    @Nonnull
    public UnitConversion getConversion2() {
        return this.conversion2;
    }

    @Override
    public double convert(double input) {
        return this.getConversion2().convert(this.getConversion1().convert(input));
    }

    @Override
    public double inverse(double input) {
        return this.getConversion1().inverse(this.getConversion2().inverse(input));
    }

    @Nonnull
    @Override
    public BigDecimal convert(@Nonnull BigDecimal input, MathContext mathContext) {
        return this.getConversion2().convert(this.getConversion1().convert(input, mathContext), mathContext);
    }

    @Nonnull
    @Override
    public BigDecimal inverse(@Nonnull BigDecimal input, MathContext mathContext) {
        return this.getConversion1().inverse(this.getConversion2().inverse(input, mathContext), mathContext);
    }
}
