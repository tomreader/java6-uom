/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.conversion;


/**
 * <p>The Unit conversion exception represents an exception thrown in the conversion of unit stage.</p>
 * @author Tom Reader
 * @version 0.1.0
 * @since 0.1.0
 */
public final class UnitConversionException extends RuntimeException {
    /**
     * Instantiates a new {@code UnitConversionException}.
     *
     * @param errorMessage the error message
     */
    public UnitConversionException(String errorMessage) {
        super(errorMessage);
    }
}
