/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.math;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.function.ThrowingRunnable;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.Random;

import static org.junit.Assert.*;

public class BigDecRationalTest {

    // Inch -> Metre conversion (* 0.0254)
    private static BigDecRational frac254Div10000;
    // Foot -> Metre conversion (* 0.3048)
    private static BigDecRational frac3048Div10000;
    // Mile -> Metre conversion (* 1609.344)
    private static BigDecRational frac1609344Div1000;

    @Before
    public void setUp() throws Exception {
        frac254Div10000 = new BigDecRational(BigDecimal.valueOf(254), BigDecimal.valueOf(10000));
        frac3048Div10000 = new BigDecRational(BigDecimal.valueOf(3048), BigDecimal.valueOf(10000));
        frac1609344Div1000 = new BigDecRational(BigDecimal.valueOf(1609344), BigDecimal.valueOf(1000));
    }

    @After
    public void tearDown() throws Exception {
        frac254Div10000 = null;
        frac3048Div10000 = null;
        frac1609344Div1000 = null;
    }

    @Test
    public void valueOfBigIntBigInt() {
        final BigInteger a = BigInteger.valueOf(300);
        BigInteger b = BigInteger.valueOf(250);
        BigDecRational aDivB = BigDecRational.valueOf(a, b);
        assertEquals(a, aDivB.getNumerator().toBigInteger());
        assertEquals(b, aDivB.getDenominator().toBigInteger());

        BigInteger c = BigInteger.valueOf(300);
        BigDecRational aDivC = BigDecRational.valueOf(a, c);
        assertEquals(BigDecimal.ONE, aDivC.getNumerator());
        assertEquals(BigDecimal.ONE, aDivC.getDenominator());

        assertThrows(ArithmeticException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                BigDecRational.valueOf(a, BigInteger.ZERO);
            }
        });
    }

    @Test
    public void valueOfLongLong() {
        long a = 300L;
        long b = 250L;
        BigDecRational aDivB = BigDecRational.valueOf(a, b);
        assertEquals(a, aDivB.getNumerator().longValue());
        assertEquals(b, aDivB.getDenominator().longValue());

        assertThrows(ArithmeticException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                BigDecRational.valueOf(300L, 0L);
            }
        });
    }

    @Test
    public void valueOfBigInteger() {
        BigInteger a = BigInteger.valueOf(300);
        BigInteger one = new BigInteger("1");

        BigDecRational rationalA = BigDecRational.valueOf(a);
        BigDecRational rationalOne = BigDecRational.valueOf(one);

        assertEquals(a, rationalA.getNumerator().toBigInteger());
        assertEquals(BigInteger.ONE, rationalA.getDenominator().toBigInteger());
        assertEquals(a, rationalA.bigIntegerValue());

        assertEquals(BigDecRational.ONE, rationalOne);
        assertEquals(BigInteger.ONE, rationalOne.bigIntegerValue());
    }

    @Test
    public void valueOfLong() {
        long a = 300L;
        long one = 1L;

        BigDecRational rationalA = BigDecRational.valueOf(a);
        BigDecRational rationalOne = BigDecRational.valueOf(one);

        assertEquals(a, rationalA.getNumerator().longValue());
        assertEquals(BigDecimal.ONE, rationalA.getDenominator());
        assertEquals(a, rationalA.longValue());

        assertEquals(BigDecRational.ONE, rationalOne);
        assertEquals(one, rationalOne.longValue());
    }

    @Test
    public void valueOfBigDecimal() {
        BigDecimal inch = new BigDecimal("0.0254");
        BigDecimal mile = new BigDecimal("1609.344");

        BigDecRational rationalInch = BigDecRational.valueOf(inch);
        BigDecRational rationalMile = BigDecRational.valueOf(mile);

        assertEquals(frac254Div10000.bigDecimalValue(), rationalInch.bigDecimalValue());
        assertEquals(frac1609344Div1000.bigDecimalValue(), rationalMile.bigDecimalValue());
    }

    @Test
    public void valueOfDouble() {
        double pi = Math.PI;
        double e = Math.E;
        double deltaDividend = Math.pow(10, 6);

        BigDecRational rationalPi = BigDecRational.valueOf(pi);
        BigDecRational rationalE = BigDecRational.valueOf(e);

        assertEquals(pi, rationalPi.doubleValue(), pi / deltaDividend);
        assertEquals(e, rationalE.doubleValue(), e / deltaDividend);
    }

    @Test
    public void valueOfBigDecimalBigDecimal() {
        final BigDecimal a = BigDecimal.valueOf(300);
        BigDecimal b = BigDecimal.valueOf(250);
        BigDecRational aDivB = BigDecRational.valueOf(a, b);
        assertEquals(a, aDivB.getNumerator());
        assertEquals(b, aDivB.getDenominator());

        BigDecimal c = BigDecimal.valueOf(300);
        BigDecRational aDivC = BigDecRational.valueOf(a, c);
        assertEquals(BigDecimal.ONE, aDivC.getNumerator());
        assertEquals(BigDecimal.ONE, aDivC.getDenominator());

        assertThrows(ArithmeticException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                BigDecRational.valueOf(a, BigDecimal.ZERO);
            }
        });
    }

    @Test
    public void bigIntegerValue() {
        BigDecimal pi = new BigDecimal("3.1415926535");
        BigDecRational rationalPi = BigDecRational.valueOf(pi);

        assertEquals(BigInteger.valueOf(3), rationalPi.bigIntegerValue());
    }

    @Test
    public void bigDecimalValue() {
        BigDecimal pi = new BigDecimal("3.1415926535");
        BigDecRational rationalPi = BigDecRational.valueOf(pi);

        assertEquals(new BigDecimal("3.1415926535"), rationalPi.bigDecimalValue());
    }

    @Test
    public void bigDecimalValueMathContext() {
        BigDecimal pi = new BigDecimal("3.1415926535");
        BigDecRational rationalPi = BigDecRational.valueOf(pi);
        MathContext mathContext = MathContext.DECIMAL32;

        assertEquals(new BigDecimal("3.1415926535").round(mathContext), rationalPi.bigDecimalValue(mathContext));
    }

    @Test
    public void simplify() {
        BigInteger oneHundred = BigInteger.valueOf(100);
        BigInteger tenThousand = BigInteger.valueOf(10000);
        BigInteger gcd1 = oneHundred.gcd(tenThousand);

        BigDecRational bigRational = BigDecRational.valueOf(
                oneHundred, tenThousand);
        BigDecRational simpBigRational = bigRational.simplify();
        assertEquals(oneHundred.divide(gcd1), simpBigRational.getNumerator().toBigInteger());
        assertEquals(tenThousand.divide(gcd1), simpBigRational.getDenominator().toBigInteger());
    }

    @Test
    public void signum() {
        BigDecRational minusOne = BigDecRational.valueOf(-1);
        BigDecRational minusOneDivMinusThree =
                new BigDecRational(BigDecimal.valueOf(-1), BigDecimal.valueOf(-3));

        assertEquals(0, BigDecRational.ZERO.signum());
        assertEquals(1, BigDecRational.ONE.signum());
        assertEquals(-1, minusOne.signum());
        assertEquals(1, minusOneDivMinusThree.signum());
    }

    @Test
    public void negate() {
        assertEquals(-1, BigDecRational.ONE.negate().signum());
        assertEquals(0, BigDecRational.ZERO.negate().signum());
        assertEquals(1, BigDecRational.valueOf(-1).negate().signum());
    }

    @Test
    public void isZero() {
        assertTrue(BigDecRational.ZERO.isZero());
        assertFalse(BigDecRational.ONE.isZero());
    }

    @Test
    public void reciprocal() {
        assertThrows(ArithmeticException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                BigDecRational.ZERO.reciprocal();
            }
        });

        assertEquals(BigDecRational.ONE, BigDecRational.ONE.reciprocal());

        assertEquals(
                BigDecRational.valueOf(BigInteger.valueOf(10000), BigInteger.valueOf(254)),
                frac254Div10000.reciprocal());
    }

    @Test
    public void isInteger() {
        assertTrue(BigDecRational.ONE.isInteger());
        assertTrue(BigDecRational.ZERO.isInteger());
        assertFalse(frac254Div10000.isInteger());
        assertFalse(frac3048Div10000.isInteger());
    }

    @Test
    public void isProper() {
        assertTrue(frac254Div10000.isProper());
        assertTrue(frac3048Div10000.isProper());
        assertFalse(BigRational.ONE.isProper());
    }

    @Test
    public void getIntegerPart() {
        BigDecimal pi = new BigDecimal("3.1415926535");
        BigDecRational rationalPi = BigDecRational.valueOf(pi);
        BigInteger three = BigInteger.valueOf(3);

        assertEquals(three, rationalPi.getIntegerPart());
    }

    @Test
    public void getProperFractionPart() {
        BigDecimal pi = new BigDecimal("3.1415926535");
        BigDecRational rationalPi = BigDecRational.valueOf(pi);
        BigDecRational rationalPiMinusThree = BigDecRational.valueOf(new BigDecimal("0.1415926535"));

        

        assertEquals(rationalPiMinusThree, rationalPi.getProperFractionPart());
    }

    @Test
    public void abs() {
        BigDecRational minusThree = BigDecRational.valueOf(-3);

        assertEquals(1, minusThree.abs().signum());
        assertEquals(-1, minusThree.signum());
    }

    @Test
    public void inc() {
        BigDecRational two = BigDecRational.valueOf(2);
        BigDecRational three = BigDecRational.valueOf(3);
        BigDecRational pi = BigDecRational.valueOf(new BigDecimal("3.1415926535"));
        BigDecRational piPlusOne = BigDecRational.valueOf(new BigDecimal("4.1415926535"));

        assertEquals(three, two.inc());
        assertEquals(piPlusOne, pi.inc());
    }

    @Test
    public void dec() {
        BigDecRational two = BigDecRational.valueOf(2);
        BigDecRational three = BigDecRational.valueOf(3);
        BigDecRational pi = BigDecRational.valueOf(new BigDecimal("3.1415926535"));
        BigDecRational piPlusOne = BigDecRational.valueOf(new BigDecimal("4.1415926535"));

        assertEquals(two, three.dec());
        assertEquals(pi, piPlusOne.dec());
    }

    @Test
    public void add() {
        BigDecRational pi = BigDecRational.valueOf(new BigDecimal("3.1415926535"));
        BigDecRational negativePi = BigDecRational.valueOf(new BigDecimal("-3.1415926535"));
        BigDecRational tau = BigDecRational.valueOf(new BigDecimal("6.283185307"));

        assertEquals(tau.bigDecimalValue(), pi.add(pi).bigDecimalValue().stripTrailingZeros());
        assertEquals(pi.bigDecimalValue(), tau.add(negativePi).bigDecimalValue());
        assertEquals(BigRational.ZERO.bigDecimalValue(), pi.add(negativePi).bigDecimalValue());
    }

    @Test
    public void subtract() {
        BigDecRational pi = BigDecRational.valueOf(new BigDecimal("3.1415926535"));
        BigDecRational negativePi = BigDecRational.valueOf(new BigDecimal("-3.1415926535"));
        BigDecRational tau = BigDecRational.valueOf(new BigDecimal("6.283185307"));

        assertEquals(BigRational.ZERO.bigDecimalValue(), pi.subtract(pi).bigDecimalValue().stripTrailingZeros());
        assertEquals(pi.bigDecimalValue(), tau.subtract(pi).bigDecimalValue().stripTrailingZeros());
        assertEquals(tau.bigDecimalValue(), pi.subtract(negativePi).bigDecimalValue().stripTrailingZeros());
    }

    @Test
    public void multiply() {
        BigDecRational pi = BigDecRational.valueOf(new BigDecimal("3.1415926535"));
        BigDecRational tau = BigDecRational.valueOf(new BigDecimal("6.283185307"));
        BigDecRational two = BigDecRational.valueOf(2);
        BigDecRational half = BigDecRational.valueOf(BigInteger.ONE, BigInteger.valueOf(2));

        assertEquals(tau.bigDecimalValue(), pi.multiply(two).bigDecimalValue().stripTrailingZeros());
        assertEquals(pi.bigDecimalValue(), tau.multiply(half).bigDecimalValue().stripTrailingZeros());
    }

    @Test
    public void divide() {
        BigDecRational pi = BigDecRational.valueOf(new BigDecimal("3.1415926535"));
        BigDecRational tau = BigDecRational.valueOf(new BigDecimal("6.283185307"));
        BigDecRational two = BigDecRational.valueOf(2);
        BigDecRational half = BigDecRational.valueOf(BigInteger.ONE, BigInteger.valueOf(2));

        assertEquals(pi.bigDecimalValue(), tau.divide(two).bigDecimalValue().stripTrailingZeros());
        assertEquals(tau.bigDecimalValue(), pi.divide(half).bigDecimalValue().stripTrailingZeros());
    }

    @Test
    public void pow() {
        BigInteger ten = BigInteger.TEN;
        BigInteger tenThousand = BigInteger.valueOf(10000);
        BigDecRational tenDivTenThousand = BigDecRational.valueOf(ten, tenThousand);
        int exponent = 4;
        BigDecRational result = BigDecRational.valueOf(
                ten.pow(exponent),
                tenThousand.pow(exponent)
        );

        assertEquals(result, tenDivTenThousand.pow(exponent));

        int negExponent = -4;
        BigDecRational negResult = BigDecRational.valueOf(
                tenThousand.pow(Math.abs(negExponent)),
                ten.pow(Math.abs(negExponent))
        );

        assertEquals(negResult, tenDivTenThousand.pow(negExponent));
    }

    @Test
    public void max() {
        BigDecRational pi = BigDecRational.valueOf(new BigDecimal("3.1415926535"));
        BigDecRational tau = BigDecRational.valueOf(new BigDecimal("6.283185307"));

        assertEquals(tau, pi.max(tau));
        assertEquals(tau, tau.max(pi));
    }

    @Test
    public void maxArray() {
        Random random = new Random();
        BigDecRational[] rationals = new BigDecRational[10];
        long startingLong = random.nextLong();
        BigDecRational startingRational = BigDecRational.valueOf(startingLong);
        long largestRational = startingLong;

        for (int i = 0; i < rationals.length; i++) {
            long nextLong = random.nextLong();
            if (nextLong > largestRational) {
                largestRational = nextLong;
            }
            rationals[i] = BigDecRational.valueOf(nextLong);
        }
        assertEquals(BigDecRational.valueOf(largestRational), startingRational.max(rationals));
    }

    @Test
    public void min() {
        BigDecRational pi = BigDecRational.valueOf(new BigDecimal("3.1415926535"));
        BigDecRational tau = BigDecRational.valueOf(new BigDecimal("6.283185307"));

        assertEquals(pi, pi.min(tau));
        assertEquals(pi, tau.min(pi));
    }

    @Test
    public void minArray() {
        Random random = new Random();
        BigDecRational[] rationals = new BigDecRational[10];
        long startingLong = random.nextLong();
        BigDecRational startingRational = BigDecRational.valueOf(startingLong);
        long smallestRational = startingLong;

        for (int i = 0; i < rationals.length; i++) {
            long nextLong = random.nextLong();
            if (nextLong < smallestRational) {
                smallestRational = nextLong;
            }
            rationals[i] = BigDecRational.valueOf(nextLong);
        }

        assertEquals(BigDecRational.valueOf(smallestRational), startingRational.min(rationals));
    }

    @Test
    public void compareTo() {
        assertEquals(0,
                BigDecRational.valueOf(BigInteger.ONE, BigInteger.ONE)
                        .compareTo(BigDecRational.ONE)
        );
        assertEquals(-1, BigDecRational.ZERO.compareTo(BigDecRational.ONE));
        assertEquals(1, BigDecRational.ONE.compareTo(BigDecRational.ZERO));
    }

    @Test
    public void testToString() {
        assertEquals("1", BigDecRational.ONE.toString());
        assertEquals("0", BigDecRational.ZERO.toString());
        assertEquals("1/3", BigDecRational.valueOf(BigInteger.ONE, BigInteger.valueOf(3)).toString());
    }

    @Test
    public void testEquals() {
        assertEquals(frac254Div10000, frac254Div10000);
        assertNotEquals(frac254Div10000, frac3048Div10000);
    }
}