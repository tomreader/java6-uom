/*
 * Copyright [2022] [Tom Reader]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.thomasreader.uom.quantity;

import com.gitlab.thomasreader.uom.conversion.Conversions;
import com.gitlab.thomasreader.uom.quantity.type.Length;
import com.gitlab.thomasreader.uom.unit.Unit;
import com.gitlab.thomasreader.uom.unit.UnitBuilder;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class QuantitiesTest {

    static List<Quantity<Length>> LENGTHS;
    static Quantity<Length>[] LENGTHS_ARRAY;
    static Quantity<Length>[] SORTED_ARRAY;
    static Unit<Length> METRE;
    static Unit<Length> INCH;
    static Unit<Length> MILE;

    @Before
    public void setUp() throws Exception {
        METRE = UnitBuilder.symbol("m").referenceUnit(Length.class);
        INCH = UnitBuilder.symbol("in").unit(METRE, Conversions.multiply(0.0254));
        MILE = UnitBuilder.symbol("mi").unit(METRE, Conversions.multiply(1609.344));

        int size = 10;

        // sorted list =
        // 100 m -> 102400 m in random units

        SORTED_ARRAY = new Quantity[] {
                METRE.of(100),
                INCH.of(7874.015748031496063),
                MILE.of(0.2485484768949336),
                MILE.of(0.4970969537898672),
                METRE.of(1600),
                METRE.of(3200),
                METRE.of(6400),
                INCH.of(503937.0078740157480315),
                METRE.of(25600),
                INCH.of(2015748.031496062992126),
                MILE.of(63.6284100851029985)
        };

        LENGTHS_ARRAY = new Quantity[] {
                SORTED_ARRAY[0],
                SORTED_ARRAY[4],
                SORTED_ARRAY[5],
                SORTED_ARRAY[9],
                SORTED_ARRAY[10],
                SORTED_ARRAY[6],
                SORTED_ARRAY[7],
                SORTED_ARRAY[8],
                SORTED_ARRAY[1],
                SORTED_ARRAY[2],
                SORTED_ARRAY[3]
//                METRE.of(100),
//                METRE.of(1600),
//                METRE.of(3200),
//
//                INCH.of(2015748.031496062992126),
//                MILE.of(63.6284100851029985),
//
//                METRE.of(6400),
//                INCH.of(503937.0078740157480315),
//                METRE.of(25600),
//
//                INCH.of(7874.015748031496063),
//                MILE.of(0.2485484768949336),
//                MILE.of(0.4970969537898672)
        };

        LENGTHS = Arrays.asList(LENGTHS_ARRAY);

    }

    @After
    public void tearDown() throws Exception {
        LENGTHS_ARRAY = null;
        LENGTHS = null;
        METRE = null;
        INCH = null;
        MILE = null;
    }

    @Test
    public void sortAsc() {
        assertArrayEquals(SORTED_ARRAY, Quantities.sortAsc(LENGTHS_ARRAY));

        assertEquals(Arrays.asList(SORTED_ARRAY), Quantities.sortAsc(LENGTHS));


    }

    @Test
    public void sortDesc() {
        Quantity<Length>[] reverse = new Quantity[SORTED_ARRAY.length];
        for (int i = 0; i < SORTED_ARRAY.length; i++) {
            int revIndex = (SORTED_ARRAY.length - 1) - i;
            reverse[revIndex] = SORTED_ARRAY[i];
        }

        assertArrayEquals(reverse, Quantities.sortDesc(LENGTHS_ARRAY));

        assertEquals(Arrays.asList(reverse), Quantities.sortDesc(LENGTHS));
    }

    @Test
    public void min() {
        assertEquals(0, METRE.of(100).compareTo(Quantities.min(LENGTHS)));
        assertEquals(0, METRE.of(100).compareTo(Quantities.min(LENGTHS_ARRAY)));
    }

    @Test
    public void max() {
        assertEquals(0, MILE.of(63.6284100851029985).compareTo(Quantities.max(LENGTHS)));
        assertEquals(0, MILE.of(63.6284100851029985).compareTo(Quantities.max(LENGTHS_ARRAY)));
    }

    @Test
    public void mean() {
        assertEquals(18609.090909090909091, Quantities.mean(LENGTHS).doubleValue(), 0.000001);
        assertEquals(18609.090909090909091, Quantities.mean(LENGTHS_ARRAY).doubleValue(), 0.000001);
    }

    @Test
    public void decimalMean() {
        MathContext mathContext = MathContext.DECIMAL32;
        assertEquals(new BigDecimal("18609.090909090909091").round(mathContext), Quantities.decimalMean(LENGTHS).bigDecimalValue().round(mathContext));
        assertEquals(new BigDecimal("18609.090909090909091").round(mathContext), Quantities.decimalMean(LENGTHS_ARRAY).bigDecimalValue().round(mathContext));
    }

    @Test
    public void median() {
        assertEquals(0, METRE.of(3200).compareTo(Quantities.median(LENGTHS)));
        assertEquals(0, METRE.of(3200).compareTo(Quantities.median(LENGTHS_ARRAY)));
    }
}